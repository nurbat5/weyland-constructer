﻿using Unity.Mathematics;
using UnityEngine;

namespace BlackEngine.Extensions
{
    public static class e_Vector3
    {
        public static float2 GetAxisWithOneVector(this Vector3 vector, int3 filter)
        {
            float worldX = filter.x == 1 ? vector.y : vector.x;
            float worldZ = filter.z == 1 ? vector.y : vector.z;
            return new Vector2(worldX, worldZ);
        }
        public static float3 GetWorldAxis(this Vector3 vector)
        {
            if ((int)vector.x != 0) return Vector3.right;
            else if ((int)vector.y != 0) return Vector3.up;
            else if ((int)vector.z != 0) return Vector3.forward;
            return Vector3.zero;
        }
        public static Vector3 Multi(this Vector3 vector, Vector3 filter) => new Vector3(vector.x * filter.x, vector.y * filter.y, vector.z * filter.z);
    }
}