﻿using System.Collections.Generic;
using UnityEngine;

namespace BlackEngine.CharacterController
{
    public enum CharacterState
    {
        Default,
        Fly
    }
    
    public struct PlayerCharacterInputs
    {
        public Vector3 MoveInputVector;
        public Vector3 LookInputVector;

        public float MoveAxisForward;
        public float MoveAxisRight;
        public Quaternion CameraRotation;
        public bool SprintHold;
        //Jump
        public bool Jump;
        //Crouch
        public bool CrouchDown;
        public bool CrouchUp;
        public bool CrouchHold;
    }

    public struct AICharacterInputs
    {
        public Vector3 MoveVector;
        public Vector3 LookVector;
    }

    public class PlayerController : CharacterCoreController
    {
        [Header("Stable Movement")]
        public float MaxStableMoveSpeed = 10f;
        public float StableMovementSharpness = 15;

        [Header("Air Movement")]
        public float MaxAirMoveSpeed = 10f;
        public float AirAccelerationSpeed = 5f;
        public float Drag = 0.1f;

        [Header("Jumping")]
        public bool AllowJumpingWhenSliding = false;
        public float JumpSpeed = 10f;
        public float JumpPreGroundingGraceTime = 0f;
        public float JumpPostGroundingGraceTime = 0f;

        [Header("Misc")]
        public List<Collider> IgnoredColliders = new List<Collider>();
        public bool OrientTowardsGravity = false;
        public Vector3 Gravity = new Vector3(0, -30f, 0);
        public Transform MeshRoot;

        [Header("Fly Mode")]
        public float flySpeed = 0.5f;
        public float flyAccelerationAmount = 3f;
        public float flyAccelerationRatio = 1f;
        public float flySlowDownRatio = 0.5f;

        public PlayerCharacterInputs CurrentInputs = new PlayerCharacterInputs();

        public float OrientationSharpness = 10;
        public OrientationMethod OrientationMethod = OrientationMethod.TowardsCamera;

        private Collider[] _probedColliders = new Collider[8];
        private Vector3 _internalVelocityAdd = Vector3.zero;
        
        //Jump
        private bool _jumpRequested = false;
        private bool _jumpConsumed = false;
        private bool _jumpedThisFrame = false;
        private float _timeSinceJumpRequested = Mathf.Infinity;
        private float _timeSinceLastAbleToJump = 0f;
        //
        private Vector3 lastInnerNormal = Vector3.zero;
        private Vector3 lastOuterNormal = Vector3.zero;

        public void ApplyInputs(ref PlayerCharacterInputs inputs)
        {
            CurrentInputs = inputs;
            CurrentInputs.MoveInputVector = Vector3.ClampMagnitude(new Vector3(CurrentInputs.MoveAxisRight, 0f, CurrentInputs.MoveAxisForward), 1f);
            Vector3 cameraPlanarDirection = Vector3.ProjectOnPlane(CurrentInputs.CameraRotation * Vector3.forward, CharacterUp).normalized;
            if (cameraPlanarDirection.sqrMagnitude == 0f)
                cameraPlanarDirection = Vector3.ProjectOnPlane(CurrentInputs.CameraRotation * Vector3.up, CharacterUp).normalized;
            Quaternion cameraPlanarRotation = Quaternion.LookRotation(cameraPlanarDirection, CharacterUp);

            switch (CurrentCharacterState)
            {
                case CharacterState.Default:
                    {
                        CurrentInputs.MoveInputVector = cameraPlanarRotation * CurrentInputs.MoveInputVector;

                        switch (OrientationMethod)
                        {
                            case OrientationMethod.TowardsCamera:
                                CurrentInputs.LookInputVector = cameraPlanarDirection;
                                break;
                            case OrientationMethod.TowardsMovement:
                                CurrentInputs.LookInputVector = CurrentInputs.MoveInputVector.normalized;
                                break;
                        }

                        // Jumping input
                        if (CurrentInputs.Jump)
                        {
                            _timeSinceJumpRequested = 0f;
                            _jumpRequested = true;
                        }

                        break;
                    }
            }
        }
        public void SetInputs(ref AICharacterInputs inputs)
        {
            CurrentInputs.MoveInputVector = inputs.MoveVector;
            CurrentInputs.LookInputVector = inputs.LookVector;
        }

        public override void UpdateRotation(ref Quaternion currentRotation, float deltaTime)
        {
            switch (CurrentCharacterState)
            {
                case CharacterState.Default:
                    {
                        if (CurrentInputs.LookInputVector != Vector3.zero && OrientationSharpness > 0f)
                        {
                            Vector3 smoothedLookInputDirection = Vector3.Slerp(CharacterForward, CurrentInputs.LookInputVector, 1 - Mathf.Exp(-OrientationSharpness * deltaTime)).normalized;
                            currentRotation = Quaternion.LookRotation(smoothedLookInputDirection, CharacterUp);
                        }
                        if (OrientTowardsGravity)
                            currentRotation = Quaternion.FromToRotation((currentRotation * Vector3.up), -Gravity) * currentRotation;
                        break;
                    }
            }
        }

        public override void UpdateVelocity(ref Vector3 currentVelocity, float deltaTime)
        {
            switch (CurrentCharacterState)
            {
                case CharacterState.Default:
                    {
                        if(Input.GetKeyDown(KeyCode.H)) TransitionToState(CharacterState.Fly);
                        Vector3 targetMovementVelocity = Vector3.zero;

                        // Ground movement
                        if (GroundingStatus.IsStableOnGround)
                        {
                            Vector3 effectiveGroundNormal = GroundingStatus.GroundNormal;
                            if (currentVelocity.sqrMagnitude > 0f && GroundingStatus.SnappingPrevented)
                            {
                                // Take the normal from where we're coming from
                                Vector3 groundPointToCharacter = TransientPosition - GroundingStatus.GroundPoint;
                                if (Vector3.Dot(currentVelocity, groundPointToCharacter) >= 0f)
                                    effectiveGroundNormal = GroundingStatus.OuterGroundNormal;
                                else effectiveGroundNormal = GroundingStatus.InnerGroundNormal;
                            }

                            // Reorient velocity on slope
                            currentVelocity = GetDirectionTangentToSurface(currentVelocity, effectiveGroundNormal) * currentVelocity.magnitude;

                            // Calculate target velocity
                            Vector3 inputRight = Vector3.Cross(CurrentInputs.MoveInputVector, CharacterUp);
                            Vector3 reorientedInput = Vector3.Cross(effectiveGroundNormal, inputRight).normalized * CurrentInputs.MoveInputVector.magnitude;
                            targetMovementVelocity = reorientedInput * MaxStableMoveSpeed;

                            // Smooth movement Velocity
                            currentVelocity = Vector3.Lerp(currentVelocity, targetMovementVelocity, 1 - Mathf.Exp(-StableMovementSharpness * deltaTime));
                        }
                        // Air movement
                        else
                        {
                            // Add move input
                            if (CurrentInputs.MoveInputVector.sqrMagnitude > 0f)
                            {
                                targetMovementVelocity = CurrentInputs.MoveInputVector * MaxAirMoveSpeed;

                                // Prevent climbing on un-stable slopes with air movement
                                if (GroundingStatus.FoundAnyGround)
                                {
                                    Vector3 perpenticularObstructionNormal = Vector3.Cross(Vector3.Cross(CharacterUp, GroundingStatus.GroundNormal), CharacterUp).normalized;
                                    targetMovementVelocity = Vector3.ProjectOnPlane(targetMovementVelocity, perpenticularObstructionNormal);
                                }

                                Vector3 velocityDiff = Vector3.ProjectOnPlane(targetMovementVelocity - currentVelocity, Gravity);
                                currentVelocity += velocityDiff * AirAccelerationSpeed * deltaTime;
                            }

                            // Gravity
                            currentVelocity += Gravity * deltaTime;

                            // Drag
                            currentVelocity *= (1f / (1f + (Drag * deltaTime)));
                        }

                        // Handle jumping
                        _jumpedThisFrame = false;
                        _timeSinceJumpRequested += deltaTime;
                        if (_jumpRequested)
                        {
                            // See if we actually are allowed to jump
                            if (!_jumpConsumed && ((AllowJumpingWhenSliding ? GroundingStatus.FoundAnyGround : GroundingStatus.IsStableOnGround) || _timeSinceLastAbleToJump <= JumpPostGroundingGraceTime))
                            {
                                // Calculate jump direction before ungrounding
                                Vector3 jumpDirection = CharacterUp;
                                if (GroundingStatus.FoundAnyGround && !GroundingStatus.IsStableOnGround)
                                    jumpDirection = GroundingStatus.GroundNormal;

                                // Makes the character skip ground probing/snapping on its next update. 
                                // If this line weren't here, the character would remain snapped to the ground when trying to jump. Try commenting this line out and see.
                                ForceUnground();

                                // Add to the return velocity and reset jump state
                                currentVelocity += (jumpDirection * JumpSpeed) - Vector3.Project(currentVelocity, CharacterUp);
                                _jumpRequested = false;
                                _jumpConsumed = true;
                                _jumpedThisFrame = true;
                            }
                        }

                        // Take into account additive velocity
                        if (_internalVelocityAdd.sqrMagnitude > 0f)
                        {
                            currentVelocity += _internalVelocityAdd;
                            _internalVelocityAdd = Vector3.zero;
                        }
                        break;
                    }
                case CharacterState.Fly:
                    {
                        if(Input.GetKeyDown(KeyCode.H)) TransitionToState(CharacterState.Default);
                        currentVelocity = Vector3.zero;

                        float ratio = 1;
                        if (Input.GetKey(KeyCode.LeftShift)) ratio = 2;
                        if (Input.GetKey(KeyCode.LeftControl)) ratio = 0.5f;
                        //if(inputs.CrouchDown) flySpeed *= slowDownRatio;
                        //if(inputs.CrouchUp) flySpeed /= slowDownRatio;

                        currentVelocity += camera.transform.forward * flySpeed * CurrentInputs.MoveAxisForward * ratio;
                        currentVelocity += camera.transform.right * flySpeed * CurrentInputs.MoveAxisRight * ratio;
                        if (Input.GetKey(KeyCode.Space)) currentVelocity += transform.up * flySpeed * 0.5f * ratio;// CurrentInputs.JumpHold
                        if (Input.GetKey(KeyCode.C)) currentVelocity += -transform.up * flySpeed * 0.5f * ratio; //CurrentInputs.CrouchDown
                        break;
                    }
            }
        }

        public override void AfterCharacterUpdate(float deltaTime)
        {
            switch (CurrentCharacterState)
            {
                case CharacterState.Default:
                    {
                        if (_jumpRequested && _timeSinceJumpRequested > JumpPreGroundingGraceTime) _jumpRequested = false;

                        if (AllowJumpingWhenSliding ? GroundingStatus.FoundAnyGround : GroundingStatus.IsStableOnGround)
                        {
                            if (!_jumpedThisFrame) _jumpConsumed = false;
                            _timeSinceLastAbleToJump = 0f;
                        }
                        else _timeSinceLastAbleToJump += deltaTime;

                        // Crouching
                        break;
                    }
            }
        }

        public override bool IsColliderValidForCollisionsCustom(Collider coll)
        {
            if(CurrentCharacterState == CharacterState.Fly) return false;
            if (IgnoredColliders.Contains(coll)) return false;
            return true;
        }
        public void AddVelocity(Vector3 velocity)
        {
            switch (CurrentCharacterState)
            {
                case CharacterState.Default:
                    {
                        _internalVelocityAdd += velocity;
                        break;
                    }
            }
        }
    }
}