using System;
using System.Threading;
using System.Collections.Generic;

public abstract class JobThread
{
    private volatile bool m_Abort = false;
    private volatile bool m_Started = false;
    private volatile bool m_DataReady = false;

    /// <summary>
    /// This is the actual job routine. override it in a concrete Job class
    /// </summary>
    protected abstract void DoWork();

    /// <summary>
    /// This is a callback which will be called from the main thread when
    /// the job has finised. Can be overridden.
    /// </summary>
    public virtual void OnFinished() { }

    public bool isAborted => m_Abort;
    public bool isStarted => m_Started;
    public bool isDataReady => m_DataReady;

    public void Execute()
    {
        m_Started = true;
        DoWork();
        m_DataReady = true;
    }

    public void AbortJob() => m_Abort = true;

    public void ResetJobState()
    {
        m_Started = false;
        m_DataReady = false;
        m_Abort = false;
    }

    public class JobWork<T> : IDisposable where T : JobThread
    {
        private class Job
        {
            private Thread m_Thread;
            private AutoResetEvent m_Event;
            private volatile bool m_Abort = false;
            public Job NextActive = null; // simple linked list to manage active threaditems
            public T Data; // the job item this thread is currently processing

            public Job()
            {
                m_Event = new AutoResetEvent(false);
                m_Thread = new Thread(ThreadMainLoop);
                m_Thread.Start();
            }

            private void ThreadMainLoop()
            {
                while (true)
                {
                    if (m_Abort) return;
                    m_Event.WaitOne();
                    if (m_Abort) return;
                    Data.Execute();
                }
            }

            public void StartJob(T aJob)
            {
                aJob.ResetJobState();
                Data = aJob;
                m_Event.Set(); //signal the thread to start working.
            }

            public void Abort()
            {
                m_Abort = true;
                if (Data != null) Data.AbortJob();
                m_Event.Set(); // signal the thread so it can finish itself.
            }

            public void Reset() => Data = null;
        }
        // internal thread pool
        private Stack<Job> m_Threads = new Stack<Job>();
        private Queue<T> m_NewJobs = new Queue<T>();
        private volatile bool m_NewJobsAdded = false;
        private Queue<T> m_Jobs = new Queue<T>();
        private Job m_Active = null; // start of the linked list of active threads

        public event Action<T> OnJobFinished;

        public JobWork(int aThreadCount)
        {
            if (aThreadCount < 1) aThreadCount = 1;
            for (int i = 0; i < aThreadCount; i++) m_Threads.Push(new Job());
        }

        public void AddJob(T aJob)
        {
            if (m_Jobs == null) throw new System.InvalidOperationException("AddJob not allowed. JobQueue has already been shutdown");
            if (aJob != null)
            {
                m_Jobs.Enqueue(aJob);
                ProcessJobQueue();
            }
        }

        public void AddJobFromOtherThreads(T aJob)
        {
            lock (m_NewJobs)
            {
                if (m_Jobs == null) throw new System.InvalidOperationException("AddJob not allowed. JobQueue has already been shutdown");
                m_NewJobs.Enqueue(aJob);
                m_NewJobsAdded = true;
            }
        }

        public int CountActiveJobs()
        {
            int count = 0;
            for (var thread = m_Active; thread != null; thread = thread.NextActive) count++;
            return count;
        }

        private void CheckActiveJobs()
        {
            Job thread = m_Active;
            Job last = null;
            while (thread != null)
            {
                Job next = thread.NextActive;
                T job = thread.Data;
                if (job.isAborted)
                {
                    if (last == null) m_Active = next;
                    else last.NextActive = next;
                    thread.NextActive = null;

                    thread.Reset();
                    m_Threads.Push(thread);
                }
                else if (thread.Data.isDataReady)
                {
                    job.OnFinished();
                    if (OnJobFinished != null) OnJobFinished(job);

                    if (last == null) m_Active = next;
                    else last.NextActive = next;
                    thread.NextActive = null;

                    thread.Reset();
                    m_Threads.Push(thread);
                }
                else last = thread;
                thread = next;
            }
        }

        private void ProcessJobQueue()
        {
            if (m_NewJobsAdded)
            {
                lock (m_NewJobs)
                {
                    while (m_NewJobs.Count > 0) AddJob(m_NewJobs.Dequeue());
                    m_NewJobsAdded = false;
                }
            }
            while (m_Jobs.Count > 0 && m_Threads.Count > 0)
            {
                var job = m_Jobs.Dequeue();
                if (!job.isAborted)
                {
                    var thread = m_Threads.Pop();
                    thread.StartJob(job);
                    // add thread to the linked list of active threads
                    thread.NextActive = m_Active;
                    m_Active = thread;
                }
            }
        }

        public void Update()
        {
            CheckActiveJobs();
            ProcessJobQueue();
        }

        public void ShutdownQueue()
        {
            for (var thread = m_Active; thread != null; thread = thread.NextActive) thread.Abort();
            while (m_Threads.Count > 0) m_Threads.Pop().Abort();
            while (m_Jobs.Count > 0) m_Jobs.Dequeue().AbortJob();
            m_Jobs = null;
            m_Active = null;
            m_Threads = null;
        }

        public void Dispose() => ShutdownQueue();
    }
}


public abstract class JobMainThread
{
    private volatile bool m_Abort = false;
    private volatile bool m_Started = false;
    private volatile bool m_DataReady = false;

    /// <summary>
    /// This is the actual job routine. override it in a concrete Job class
    /// </summary>
    protected abstract void DoWork();

    /// <summary>
    /// This is a callback which will be called from the main thread when
    /// the job has finised. Can be overridden.
    /// </summary>
    public virtual void OnFinished() { }

    public bool isAborted => m_Abort;
    public bool isStarted => m_Started;
    public bool isDataReady => m_DataReady;

    public void Execute()
    {
        m_Started = true;
        DoWork();
        m_DataReady = true;
    }

    public void AbortJob() => m_Abort = true;

    public void ResetJobState()
    {
        m_Started = false;
        m_DataReady = false;
        m_Abort = false;
    }

    public class JobWork<T> : IDisposable where T : JobThread
    {
        private class Job
        {
            //private Thread m_Thread;
            private AutoResetEvent m_Event;
            private volatile bool m_Abort = false;
            public Job NextActive = null; // simple linked list to manage active threaditems
            public T Data; // the job item this thread is currently processing

            public Job()
            {
                m_Event = new AutoResetEvent(false);
                //m_Thread = new Thread(ThreadMainLoop);
                //m_Thread.Start();
            }

            private void ThreadMainLoop()
            {
                while (true)
                {
                    if (m_Abort) return;
                    m_Event.WaitOne();
                    if (m_Abort) return;
                    Data.Execute();
                }
            }

            public void StartJob(T aJob)
            {
                aJob.ResetJobState();
                Data = aJob;
                m_Event.Set(); //signal the thread to start working.
            }

            public void Abort()
            {
                m_Abort = true;
                if (Data != null) Data.AbortJob();
                m_Event.Set(); // signal the thread so it can finish itself.
            }

            public void Reset() => Data = null;
        }
        // internal thread pool
        private Stack<Job> m_Threads = new Stack<Job>();
        private Queue<T> m_NewJobs = new Queue<T>();
        private volatile bool m_NewJobsAdded = false;
        private Queue<T> m_Jobs = new Queue<T>();
        private Job m_Active = null; // start of the linked list of active threads

        public event Action<T> OnJobFinished;

        public JobWork(int aThreadCount)
        {
            if (aThreadCount < 1) aThreadCount = 1;
            for (int i = 0; i < aThreadCount; i++) m_Threads.Push(new Job());
        }

        public void AddJob(T aJob)
        {
            if (m_Jobs == null) throw new System.InvalidOperationException("AddJob not allowed. JobQueue has already been shutdown");
            if (aJob != null)
            {
                m_Jobs.Enqueue(aJob);
                ProcessJobQueue();
            }
        }

        public void AddJobFromOtherThreads(T aJob)
        {
            lock (m_NewJobs)
            {
                if (m_Jobs == null) throw new System.InvalidOperationException("AddJob not allowed. JobQueue has already been shutdown");
                m_NewJobs.Enqueue(aJob);
                m_NewJobsAdded = true;
            }
        }

        public int CountActiveJobs()
        {
            int count = 0;
            for (var thread = m_Active; thread != null; thread = thread.NextActive) count++;
            return count;
        }

        private void CheckActiveJobs()
        {
            Job thread = m_Active;
            Job last = null;
            while (thread != null)
            {
                Job next = thread.NextActive;
                T job = thread.Data;
                if (job.isAborted)
                {
                    if (last == null) m_Active = next;
                    else last.NextActive = next;
                    thread.NextActive = null;

                    thread.Reset();
                    m_Threads.Push(thread);
                }
                else if (thread.Data.isDataReady)
                {
                    job.OnFinished();
                    if (OnJobFinished != null) OnJobFinished(job);

                    if (last == null) m_Active = next;
                    else last.NextActive = next;
                    thread.NextActive = null;

                    thread.Reset();
                    m_Threads.Push(thread);
                }
                else last = thread;
                thread = next;
            }
        }

        private void ProcessJobQueue()
        {
            if (m_NewJobsAdded)
            {
                lock (m_NewJobs)
                {
                    while (m_NewJobs.Count > 0) AddJob(m_NewJobs.Dequeue());
                    m_NewJobsAdded = false;
                }
            }
            while (m_Jobs.Count > 0 && m_Threads.Count > 0)
            {
                var job = m_Jobs.Dequeue();
                if (!job.isAborted)
                {
                    var thread = m_Threads.Pop();
                    thread.StartJob(job);
                    // add thread to the linked list of active threads
                    thread.NextActive = m_Active;
                    m_Active = thread;
                }
            }
        }

        public void Update()
        {
            CheckActiveJobs();
            ProcessJobQueue();
        }

        public void ShutdownQueue()
        {
            for (var thread = m_Active; thread != null; thread = thread.NextActive) thread.Abort();
            while (m_Threads.Count > 0) m_Threads.Pop().Abort();
            while (m_Jobs.Count > 0) m_Jobs.Dequeue().AbortJob();
            m_Jobs = null;
            m_Active = null;
            m_Threads = null;
        }

        public void Dispose() => ShutdownQueue();
    }
}

