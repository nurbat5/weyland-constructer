﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weyland
{
    public class ObjectManager : MonoBehaviour
    {
        public ObjectManagerFind objectManagerFind = new ObjectManagerFind(new Transform[0]);
        public static ObjectManager instance;
        public static Transform currentSelection { get; private set; }

        void Awake() => instance = this;
        void Update()
        {
            if(currentSelection != objectManagerFind.Find())
            {
                // if (_currentSelection != null) OnDeselect(_currentSelection);
                // if (responsiveSelector.selection != null) OnSelect(responsiveSelector.selection);
            }
            currentSelection = objectManagerFind.transform;
        }

        // public void OnSelect(Transform selection)
        // {
        //     // var outline = selection.GetComponent<Outline>();
        //     // if (outline != null)
        //     // {
        //     //     outline.OutlineMode = OutlineMode;
        //     //     outline.OutlineColor = outlineColor;
        //     //     outline.OutlineWidth = outlineWidth;
        //     // }
        // }
        // public void OnDeselect(Transform selection)
        // {
        //     // var outline = selection.GetComponent<Outline>();
        //     // if (outline != null) outline.OutlineWidth = 0;
        // }

    }

    [Serializable]
    public struct ObjectManagerFind
    {
        [SerializeField] private List<Transform> _arrayObjects;
        [SerializeField] private float _threshold;
        public Transform transform;

        public ObjectManagerFind(IEnumerable<Transform> arrayObjects) : this(arrayObjects, 0.97f) { }
        public ObjectManagerFind(IEnumerable<Transform> selectables, float threshold)
        {
            this.transform = null;
            this._threshold = threshold;
            this._arrayObjects = new List<Transform>(selectables);
        }

        public void Remove(Transform transform) => _arrayObjects.Remove(transform);
        public void Add(Transform transform) => _arrayObjects.Add(transform);

        public Transform Find() => Find(Camera.main.ScreenPointToRay(Input.mousePosition));
        public Transform Find(Ray ray)
        {
            transform = null;
            var closest = 0f;

            for (int i = 0; i < _arrayObjects.Count; i++)
            {
                var vector1 = ray.direction;
                var vector2 = _arrayObjects[i].transform.position - ray.origin;

                var lookPercentage = Vector3.Dot(vector1.normalized, vector2.normalized);

                if (lookPercentage > _threshold && lookPercentage > closest)
                {
                    closest = lookPercentage;
                    transform = _arrayObjects[i].transform;
                    return transform;
                }
            }
            return null;
        }
    }
}