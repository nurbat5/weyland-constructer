using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Weyland.Languages
{
    public class Language
    {
        public List<int> languageSupport { get; private set; } = new List<int>();
        public LanguageData dataLanguages { get; private set; }

        // USE LANGUAGE
        public static string Read(string key) => instance.dataLanguages.Read(key).text;
        public static string Read(string key, string text) => instance.dataLanguages.Read(key, text).text;
        public static string Write(string key, string text) => instance.dataLanguages.Write(key, text).text;
        public static int[] AllLanguages() => instance.languageSupport.ToArray();

        // LOAD LANGUAGE
        public static bool hasInstance { get; private set; } = false;
        public static Language instance => hasInstance ? _instance : new Language();

        private static Language _instance;
        public Language()
        {
            // INSTANCE
            hasInstance = true;
            _instance = this;

            // CONFIG
            dataLanguages = ScriptableObject.CreateInstance<LanguageData>();
            LoadLanguage(PlayerData.Read("Weyland.Languages.defaultLanguage", SystemLanguage.English));
        }

        private void LoadResource(SystemLanguage language)
        {
            LanguageData[] resources = Resources.LoadAll<LanguageData>("Data/Languages");
            for (int index = 0; index < resources.Length; index++)
            {
                // ADD SUPPORT LANGUAGE
                if (resources[index].language != language)
                {
                    if (languageSupport.Exists(u => u == (int)resources[index].language)) continue;
                    languageSupport.Add((int)resources[index].language);
                    continue;
                }

                // ADD LANGUAGE
                if(dataLanguages.language != language) 
                {
                    languageSupport.Add((int)resources[index].language);
                    dataLanguages = resources[index];
                    continue;
                }

                // UPDATE LANGUAGE
                if(dataLanguages.language == language) OverwriteLanguage(resources[index]);
            }
        }
        public bool LoadLanguage(SystemLanguage language)
        {
            bool isLoaded = false;
            string path = $"{Application.dataPath}/Data/Languages/";
            if (!Directory.Exists(path)) return false;

            string[] filesPath = Directory.GetFiles(path, "*.json");
            for (int index = 0; index < filesPath.Length; index++)
            {
                LanguageData tempData = ScriptableObject.CreateInstance<LanguageData>();
                JsonUtility.FromJsonOverwrite(File.ReadAllText(filesPath[index]), tempData);

                // ADD SUPPORT LANGUAGE
                if (language != tempData.language)
                {
                    if (languageSupport.Exists(u => u == (int)tempData.language)) continue;
                    languageSupport.Add((int)tempData.language);
                    continue;
                }

                // SETUP LANGUAGE
                languageSupport.Add((int)tempData.language);
                dataLanguages = tempData;
                isLoaded = true;
            }
            LoadResource(language);
            return isLoaded;
        }
        private void OverwriteLanguage(LanguageData resource)
        {
            bool isWrite = false;
            resource.dataKeys.ForEach(data =>
            {
                if(!dataLanguages.IsKey(data.key))
                {
                    if(!isWrite) isWrite = true;
                    dataLanguages.Write(data.key, data.text, false);
                }
            });
            if(isWrite) dataLanguages.Save();
        }

    }
}