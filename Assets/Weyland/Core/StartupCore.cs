using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Weyland
{
    public class BackgroundEngine : MonoBehaviour
    {
        public UnityEvent updateEvent;
        public UnityEvent fixedUpdateEvent;
        public UnityEvent onEnableEvent;
        public UnityEvent onDisableEvent;
        public UnityEvent onDestroyEvent;


        public IStartupComponent startupComponent => _startupComponent;
        private StartupComponent _startupComponent = new StartupComponent(new List<IStartup>());
        public struct StartupComponent : IStartupComponent
        {
            private List<IStartup> _arrayStartup;
            public StartupComponent(List<IStartup> arrayStartup) { _arrayStartup = arrayStartup; }
            public void Clear()
            {
                _arrayStartup.ForEach(item => item.Dispose());
                _arrayStartup.Clear();
            }
            public void Add<T>() where T : IStartup, new()
            {
                T component = new T();
                _arrayStartup.Add(component);
                component.Init();
            }
            public void Remove<T>() where T : IStartup
            {
                IStartup component = _arrayStartup.Find(u => u is T);
                if(component != null) _arrayStartup.Remove(component);
            }
            public void Remove(IStartup Startup)
            {
                _arrayStartup.Remove(Startup);
                Startup.Dispose();
            }

            internal IEnumerable<IStartup> StartupOnSystems 
            {
                get => _arrayStartup;
                set => _arrayStartup = value as List<IStartup>;
            }
            internal void Update() => _arrayStartup.ForEach(item => item.Update());
            internal void FixedUpdate() => _arrayStartup.ForEach(item => item.FixedUpdate());
            internal void OnEnable() => _arrayStartup.ForEach(item => item.OnEnable());
            internal void OnDisable() => _arrayStartup.ForEach(item => item.OnDisable());
            internal void OnDestroy() => _arrayStartup.ForEach(item => item.Dispose());
        }

        private void Update()
        {
            _startupComponent.Update();
            updateEvent.Invoke();
        } 
        private void FixedUpdate() 
        {
            _startupComponent.FixedUpdate();
            fixedUpdateEvent.Invoke();
        }
        private void OnEnable() 
        {
            _startupComponent.OnEnable();
            onEnableEvent.Invoke();
        }
        private void OnDisable() 
        {
            _startupComponent.OnDisable();
            onDisableEvent.Invoke();
        }
        private void OnDestroy()
        {
            _startupComponent.OnDestroy();
            onDestroyEvent.Invoke();
        }
    }
}