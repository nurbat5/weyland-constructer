using System;

namespace Weyland
{
    public interface IStartup : IDisposable
    {
        void FixedUpdate();
        void Update();
        void Init();
        void OnEnable();
        void OnDisable();
    }
}