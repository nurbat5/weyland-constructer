using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Weyland
{
    public partial class Log
    {
        public enum Warning
        {
            Info,
            Error,
            Warning
        }
        #if UNITY_EDITOR
        public static bool hasSave = false;
        #else
        public static bool hasSave = true;
        #endif
        
        private static void SaveLog(string message, string fileName = "error")
        {
            if(!hasSave) return;
            using (StreamWriter sw = new StreamWriter($"{Application.dataPath}/{fileName}.log", true, System.Text.Encoding.Default))
            {
                sw.WriteLine(message);
            }
        }

        public static void Write(object message) => Write(message.ToString());
        public static void Write(Exception e) => Write(e.Message + e.StackTrace, Warning.Warning);
        public static void Write(IEnumerable collection) { foreach(var value in collection) Write(value); }
        public static void Write(string message, Warning warning = Warning.Info)
        {
            SaveLog(message.ToString());
            switch(warning)
            {
                case Warning.Warning:
                    Debug.LogWarning(message);
                    break;
              case Warning.Error:
                     Debug.LogError(message);
                    break;
               case Warning.Info:
                    Debug.Log(message);
                    break;
            }
        }
    }
}