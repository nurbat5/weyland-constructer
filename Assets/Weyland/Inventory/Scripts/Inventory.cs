﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using Weyland.Builder;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Weyland.InventorySystem
{
    [DisallowMultipleComponent]
    public class Inventory : MonoBehaviour
    {
        // EVENT
        public class AssetEvent : UnityEvent<InventoryElementAsset> {} // Event init class
        public static AssetEvent elementOnChanged { get; set; } = new AssetEvent(); // Event every call on selected
        public static Inventory instance { get; private set; }

        // CONFIG INVENTORY
        public Transform contentParent; // parent to attach category element
        public GameObject categoryPrefab;
        public GameObject elementPrefab;
        public static List<InventoryElement> categoryArray = new List<InventoryElement>();

        public void LoadResource()
        {
            InventoryElementAsset[] assets = Resources.LoadAll<InventoryElementAsset>("System/Inventory");
            foreach(var asset in assets)
            {
                asset.textureImage = asset.textureImage == null ? Resources.Load<Texture2D>($"System/Inventory/{asset.categoryName}/Images/{asset.name}") : asset.textureImage;
                CreateCategory(asset.categoryName).CreateElement(asset);
            }
        }

        private void OnEnable() 
        {
            categoryArray.Clear();
            PreInit();
            // for(int i = 0; i < categoryArray.Count; i++)
            // {
            //     var inventoryElement = new InventoryElement(categoryPrefab, elementPrefab, contentParent, categoryArray[i].name);
            //     categoryArray[i].elements.ForEach(inventoryElementAsset => inventoryElement.CreateElement(inventoryElementAsset));
            //     categoryArray[i] = inventoryElement;
            // }
            LoadResource();
            MaterialBuilderChanged.ReadMaterialDirectory();
        }

        // CATEGORY ELEMENT
        public static InventoryElement CreateCategory(string categoryName)
        {
            // IF CHECK IS CREATED
            InventoryElement tempInventoryCategoryItem = categoryArray.Find(u => u.name == categoryName);
            if (tempInventoryCategoryItem.gameObjectParent) return tempInventoryCategoryItem;

            // IF NOT, CREATED
            tempInventoryCategoryItem = new InventoryElement(instance.categoryPrefab, instance.elementPrefab, instance.contentParent, categoryName);
            categoryArray.Add(tempInventoryCategoryItem);
            return tempInventoryCategoryItem;
        }
        public static bool showWindow 
        { 
            get => instance.transform.GetChild(0).gameObject.activeSelf; 
            set => instance.transform.GetChild(0).gameObject.SetActive(value); 
        }

        private void PreInit()
        {
            instance = this;
            showWindow = false;
        }
        private void Update() { if(Keyboard.current.iKey.wasReleasedThisFrame) WindowGame.OpenOrClose(WindowGame.TypeWindow.Inventory); }
    }
}
