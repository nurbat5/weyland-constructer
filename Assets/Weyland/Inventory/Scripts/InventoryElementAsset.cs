using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Weyland.InventorySystem
{
    [CreateAssetMenu(menuName = "Weyland/InventoryElement", fileName = "Element")]
    public class InventoryElementAsset : ScriptableObject
    {
        public string categoryName;
        public Texture textureImage;
        public Material materialImage;

        //System
        #region systemParameters

        private GameObject _gameObjectParent;
        public GameObject gameObjectParent
        {
            get => _gameObjectParent;
            set
            {
                _gameObjectParent = value;
                _gameObjectParent.GetComponentInChildren<RawImage>().texture = textureImage;
                _gameObjectParent.GetComponentInChildren<RawImage>().material = materialImage;
                _gameObjectParent.GetComponentInChildren<Text>().text = name;
            }
        }

        private InventoryElement _categoryParent;
        public InventoryElement categoryParent 
        { 
            get => _categoryParent;
            internal set 
            {
                categoryName = value.name;
                _categoryParent = value;
            }
        }

        public InventoryElementAsset() 
        { 
            categoryParent = new InventoryElement();
        }
        public InventoryElementAsset(string name, string categoryName, object contentData, Texture2D textureImage = null) : this()
        {
            this.name = name;
            this.categoryName = categoryName;
            this._contentData = contentData;
            this.textureImage = textureImage;
        }
        public InventoryElementAsset(string name, string categoryName, object contentData, Material materialImage) : this(name, categoryName, contentData)
        {
            this.materialImage = materialImage;
        }

        private object _contentData;
        public T ContentData<T>(T data) => (T)(_contentData = (object)data);
        public T ContentData<T>() => (T)_contentData;

        #endregion
    }
}