using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Weyland.InventorySystem
{
    [Serializable]
    public struct InventoryElement
    {
        public string name; // Category name
        public GameObject gameObjectParent { get; set; } // current transform object
        public Transform itemsParent { get; set; } // attach to elements
        public List<InventoryElementAsset> elements; // Elements
        private GameObject _elementPrefab { get; set; }

        public InventoryElement(GameObject categoryPrefab, GameObject elementPrefab, Transform contentParent, string categoryName)
        {
            _elementPrefab = elementPrefab;
            elements = new List<InventoryElementAsset>();
            gameObjectParent = MonoBehaviour.Instantiate(categoryPrefab, contentParent); // Init Prefab
            gameObjectParent.transform.SetParent(contentParent); // Set parent category
            gameObjectParent.GetComponentInChildren<Text>().text = name = categoryName; // Set Category name
            itemsParent = gameObjectParent.transform.Find("Items");
        }
        public void CreateElement(InventoryElementAsset asset)
        {
            asset.categoryParent = this;
            asset.gameObjectParent = MonoBehaviour.Instantiate(_elementPrefab, itemsParent);
            asset.gameObjectParent.transform.SetParent(itemsParent);
            asset.gameObjectParent.GetComponent<Button>().onClick.AddListener(() => ItemClicked(asset));
            elements.Add(asset);
        }
        public void Clear()
        {
            elements.ForEach(item => MonoBehaviour.Destroy(item.gameObjectParent));
            elements.Clear();
        }
        public void Remove()
        {
            Inventory.categoryArray.Remove(this);
            MonoBehaviour.Destroy(gameObjectParent);
        }

        // EVENT ON CHANGE STATUS
        public static InventoryElementAsset inventoryElement { get; private set; }
        private static Button _button;
        private static ColorBlock _buttonColors;
        public static void ItemClicked(InventoryElementAsset asset)
        {
            if(_button) _button.colors = _buttonColors;
            _button = null;

            if(asset && asset.gameObjectParent)
            {
                _button = asset.gameObjectParent.GetComponent<Button>();
                var tmpColors = (_buttonColors = _button.colors);
                tmpColors.normalColor = new Color()
                {
                    g = 1,
                    a = 0.0039f * 100
                };
                _button.colors = tmpColors;
            }
            Inventory.elementOnChanged.Invoke(inventoryElement = asset);
        }
    }
}