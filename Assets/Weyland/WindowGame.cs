﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Weyland.GameSettings;
using Weyland.InputSystem;
using Weyland.InventorySystem;

namespace Weyland
{
    public class WindowGame : MonoBehaviour
    {
        private List<TypeWindow> windowCurrent = new List<TypeWindow>();
        public static bool isWindowsOpens => instance.windowCurrent.Count > 0;

        public enum TypeWindow
        {
            Inventory,
            GameOptions,
            ToolsSettings
        }

        public static WindowGame instance;
        private void Awake()
        {
            instance = this;
        }

        private void Update()
        {
            InputManager.activeMouse = isWindowsOpens;
            if (Keyboard.current.escapeKey.wasReleasedThisFrame)
            {
                if(isWindowsOpens) Close(windowCurrent[windowCurrent.Count - 1]);
                else Open(TypeWindow.GameOptions);
            } 
        }

        public static void OpenOrClose(TypeWindow typeWindow)
        {
            if (instance.windowCurrent.Exists(u => u == typeWindow)) Close(typeWindow);
            else Open(typeWindow);
        }

        public static void Open(TypeWindow typeWindow)
        {
            if (instance.windowCurrent.Exists(u => u == typeWindow)) return;

            if (typeWindow == TypeWindow.Inventory) Inventory.showWindow = true;
            else if (typeWindow == TypeWindow.GameOptions) GameOptions.showWindow = true;
            else if (typeWindow == TypeWindow.ToolsSettings) BuilderConfigure.showWindow = true;

            instance.windowCurrent.Add(typeWindow);
        }

        public static void Close(TypeWindow typeWindow)
        {
            if (!instance.windowCurrent.Exists(u => u == typeWindow)) return;

            if (typeWindow == TypeWindow.Inventory) Inventory.showWindow = false;
            else if (typeWindow == TypeWindow.GameOptions) GameOptions.showWindow = false;
            else if (typeWindow == TypeWindow.ToolsSettings) BuilderConfigure.showWindow = false;

            instance.windowCurrent.Remove(typeWindow);
        }
    }
}
