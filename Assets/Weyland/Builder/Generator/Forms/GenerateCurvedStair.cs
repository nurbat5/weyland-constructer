﻿// Copyright (c) 2019 All Rights Reserved
// Dmitriy Annenkov
// 03 18 2019

using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ProBuilder;
using Weyland;
using Weyland.Builder;
using static UnityEngine.UI.Dropdown;
using Debug = UnityEngine.Debug;
using BuilderMesh = UnityEngine.ProBuilder.ProBuilderMesh;
using Weyland.Serializations;
using Weyland.Languages;

namespace Weyland.Builder
{
    public class GenerateCurvedStair : GenerateMeshBehaviour
    {
        private PivotLocation pivotType = PivotLocation.FirstVertex;
        private float stairWidth = 2;
        private float height = 2;
        private float innerRadius = 40;
        private float circumference = 5;
        private int steps = 10;
        private bool buildSides = true;

        public override void PreInit()
        {
            string _namespace = this.GetType().FullName;

            pivotType = PlayerData.Read(_namespace + ".pivotType", pivotType);
            stairWidth = PlayerData.Read(_namespace + ".stairWidth", stairWidth);
            height = PlayerData.Read(_namespace + ".height", height);
            innerRadius = PlayerData.Read(_namespace + ".innerRadius", innerRadius);
            circumference = PlayerData.Read(_namespace + ".circumference", circumference);
            steps = PlayerData.Read(_namespace + ".steps", steps);
            buildSides = PlayerData.Read(_namespace + ".buildSides", buildSides);

            GenerateMenu(_namespace);
            MeshGenerate();
        }

        private void GenerateMenu(string _namespace)
        {
            BuilderConfigure.title = Language.Read(_namespace, "Stair");

            BuilderConfigure.GenerateEnum(
                 Language.Read("Weyland.Builder.pivotType"),
                 Language.Read("Weyland.Builder.pivotType.info"), typeof(PivotLocation), (int)pivotType).AddListener((int value) =>
                 {
                     pivotType = PlayerData.Write(_namespace + ".pivotLocation", (PivotLocation)value);
                     MeshGenerate();
                 });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".stairWidth"),
                Language.Read(_namespace + ".stairWidth.info"), stairWidth).AddListener((float value) =>
                {
                    stairWidth = PlayerData.Write(_namespace + ".stairWidth", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".height", "Height"),
                Language.Read(_namespace + ".height.info", "The height of the stair set."), height).AddListener((float value) =>
                {
                    height = PlayerData.Write(_namespace + ".height", value);
                    MeshGenerate();
                });
 
            BuilderConfigure.Generate(
                Language.Read(_namespace + ".innerRadius", "Inner Radius"),
                Language.Read(_namespace + ".innerRadius.info", "The radius from center to inner stair bounds."), innerRadius).AddListener((float value) =>
                {
                    innerRadius = PlayerData.Write(_namespace + ".innerRadius", value);
                    MeshGenerate();
                });
 
            BuilderConfigure.Generate(
                Language.Read(_namespace + ".circumference", "Circumference"),
                Language.Read(_namespace + ".circumference.info", "The amount of curvature in degrees."), circumference).AddListener((float value) =>
                {
                    circumference = PlayerData.Write(_namespace + ".circumference", value);
                    MeshGenerate();
                });
 
            BuilderConfigure.Generate(
                Language.Read(_namespace + ".steps", "Steps"),
                Language.Read(_namespace + ".steps.info", "How many steps this stair set contains."), steps).AddListener((int value) =>
                {
                    steps = PlayerData.Write(_namespace + ".steps", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".buildSides", "Build Sides"),
                Language.Read(_namespace + ".buildSides.info", "If true, build the side and back walls. If false, only the stair top and connecting planes will be built."), buildSides).AddListener((bool value) =>
                {
                    buildSides = PlayerData.Write(_namespace + ".buildSides", value);
                    MeshGenerate();
                });
        }
        private void MeshGenerate() => Generate(ShapeGenerator.GenerateCurvedStair(pivotType, stairWidth, height, innerRadius, circumference, steps, buildSides));
    }
}