﻿// Copyright (c) 2019 All Rights Reserved
// Dmitriy Annenkov
// 03 18 2019

using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ProBuilder;
using Weyland;
using Weyland.Builder;
using static UnityEngine.UI.Dropdown;
using Debug = UnityEngine.Debug;
using BuilderMesh = UnityEngine.ProBuilder.ProBuilderMesh;
using Weyland.Serializations;
using Weyland.Languages;

namespace Weyland.Builder
{
    public class GenerateArch : GenerateMeshBehaviour
    {
        private PivotLocation pivotType = PivotLocation.FirstVertex;
        private float angle = 180;
        private float radius = 3;
        private float width = 0.5f;
        private float depth = 1;
        private int radialCuts = 6;
        private bool insideFaces = true;
        private bool outsideFaces = true;
        private bool frontFaces = true;
        private bool backFaces = true;
        private bool endCaps = true;

        public override void PreInit()
        {
            string _namespace = this.GetType().FullName;

            pivotType = PlayerData.Read(_namespace + ".pivotLocation", pivotType);
            angle = PlayerData.Read(_namespace + ".angle", angle);
            radius = PlayerData.Read(_namespace + ".radius", radius);
            width = PlayerData.Read(_namespace + ".width", width);
            depth = PlayerData.Read(_namespace + ".depth", depth);
            radialCuts = PlayerData.Read(_namespace + ".radialCuts", radialCuts);
            insideFaces = PlayerData.Read(_namespace + ".insideFaces", insideFaces);
            outsideFaces = PlayerData.Read(_namespace + ".outsideFaces", outsideFaces);
            frontFaces = PlayerData.Read(_namespace + ".frontFaces", frontFaces);
            backFaces = PlayerData.Read(_namespace + ".backFaces", backFaces);
            endCaps = PlayerData.Read(_namespace + ".endCaps", endCaps);

            GenerateMenu(_namespace);
            MeshGenerate();
        }

        private void GenerateMenu(string _namespace)
        {
            BuilderConfigure.title = Language.Read(_namespace);

            BuilderConfigure.GenerateEnum(
                 Language.Read("Weyland.Builder.pivotType"),
                 Language.Read("Weyland.Builder.pivotType.info"), typeof(PivotLocation), (int)pivotType).AddListener((int value) =>
                 {
                     pivotType = PlayerData.Write(_namespace + ".pivotLocation", (PivotLocation)value);
                     MeshGenerate();
                 });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".angle"),
                Language.Read(_namespace + ".angle.info"), angle).AddListener((float value) =>
                {
                    angle = PlayerData.Write(_namespace + ".angle", value);
                    MeshGenerate();
                });


            BuilderConfigure.Generate(
                Language.Read(_namespace + ".radius"),
                Language.Read(_namespace + ".radius.info"), radius).AddListener((float value) =>
                {
                    radius = PlayerData.Write(_namespace + ".radius", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".width"),
                Language.Read(_namespace + ".width.info"), width).AddListener((float value) =>
                {
                    width = PlayerData.Write(_namespace + ".width", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".depth"),
                Language.Read(_namespace + ".depth.info"), depth).AddListener((float value) =>
                {
                    depth = PlayerData.Write(_namespace + ".depth", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".radialCuts"),
                Language.Read(_namespace + ".radialCuts.info"), radialCuts).AddListener((int value) =>
                {
                    radialCuts = PlayerData.Write(_namespace + ".radialCuts", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".insideFaces"),
                Language.Read(_namespace + ".insideFaces.info"), insideFaces).AddListener((bool value) =>
                {
                    insideFaces = PlayerData.Write(_namespace + ".insideFaces", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".outsideFaces"),
                Language.Read(_namespace + ".outsideFaces.info"), outsideFaces).AddListener((bool value) =>
                {
                    outsideFaces = PlayerData.Write(_namespace + ".outsideFaces", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".frontFaces"),
                Language.Read(_namespace + ".frontFaces.info"), frontFaces).AddListener((bool value) =>
                {
                    frontFaces = PlayerData.Write(_namespace + ".frontFaces", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".backFaces"),
                Language.Read(_namespace + ".backFaces.info"), backFaces).AddListener((bool value) =>
                {
                    backFaces = PlayerData.Write(_namespace + ".backFaces", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".endCaps"),
                Language.Read(_namespace + ".endCaps.info"), endCaps).AddListener((bool value) =>
                {
                    endCaps = PlayerData.Write(_namespace + ".endCaps", value);
                    MeshGenerate();
                });
        }
        private void MeshGenerate() => Generate(ShapeGenerator.GenerateArch(pivotType, angle, radius, width, depth, radialCuts, insideFaces, outsideFaces, frontFaces, backFaces, endCaps));
    }
}