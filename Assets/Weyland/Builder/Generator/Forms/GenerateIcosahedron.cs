﻿// Copyright (c) 2019 All Rights Reserved
// Dmitriy Annenkov
// 03 18 2019

using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ProBuilder;
using Weyland;
using Weyland.Builder;
using static UnityEngine.UI.Dropdown;
using Debug = UnityEngine.Debug;
using BuilderMesh = UnityEngine.ProBuilder.ProBuilderMesh;
using Weyland.Serializations;
using Weyland.Languages;

namespace Weyland.Builder
{
    public class GenerateIcosahedron : GenerateMeshBehaviour
    {
        private PivotLocation pivotType = PivotLocation.Center;
        private float radius = 1;
        private int subdivisions = 1;
        private bool weldVertices = true;
        private bool manualUvs = true;

        public override void PreInit()
        {
            string _namespace = this.GetType().FullName;

            pivotType = PlayerData.Read(_namespace + ".pivotType", pivotType);
            radius = PlayerData.Read(_namespace + ".radius", radius);
            subdivisions = PlayerData.Read(_namespace + ".subdivisions", subdivisions);
            weldVertices = PlayerData.Read(_namespace + ".weldVertices", weldVertices);
            manualUvs = PlayerData.Read(_namespace + ".manualUvs", manualUvs);

            GenerateMenu(_namespace);
            MeshGenerate();
        }

        private void GenerateMenu(string _namespace)
        {
            BuilderConfigure.title = Language.Read(_namespace, "Sphere");

            BuilderConfigure.GenerateEnum(
                 Language.Read( "Weyland.Builder.pivotType"),
                 Language.Read( "Weyland.Builder.pivotType.info"), typeof(PivotLocation), (int)pivotType).AddListener((int value) =>
                 {
                     pivotType = PlayerData.Write(_namespace + ".pivotLocation", (PivotLocation)value);
                     MeshGenerate();
                 });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".radius", "Radius"),
                Language.Read(_namespace + ".radius.info", "The radius of the sphere."), radius).AddListener((float value) =>
                {
                    radius = PlayerData.Write(_namespace + ".radius", value);
                    MeshGenerate();
                });


            BuilderConfigure.Generate(
                Language.Read(_namespace + ".subdivisions", "Subdivisions"),
                Language.Read(_namespace + ".subdivisions.info", "How many subdivisions to perform."), subdivisions).AddListener((int value) =>
                {
                    subdivisions = PlayerData.Write(_namespace + ".subdivisions", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".weldVertices"),
                Language.Read(_namespace + @".weldVertices.info", "If false this function will not extract shared indexes."+
                "This is useful when showing a preview, where" +
                "speed of generation is more important than making the shape editable."),  weldVertices).AddListener((bool value) =>
                {
                    weldVertices = PlayerData.Write(_namespace + ".weldVertices", value);
                    MeshGenerate();
                });
 
 
            BuilderConfigure.Generate(
                Language.Read(_namespace + ".manualUvs", "Manual UV"),
                Language.Read(_namespace + ".manualUvs.info", "For performance reasons faces on icospheres are marked as manual UVs. Pass false to this parameter to force auto unwrapped UVs."), manualUvs).AddListener((bool value) =>
                {
                    manualUvs = PlayerData.Write(_namespace + ".manualUvs", value);
                    MeshGenerate();
                });
 
        }
        private void MeshGenerate() => Generate(ShapeGenerator.GenerateIcosahedron(pivotType, radius, subdivisions, weldVertices, manualUvs));
    }
}