﻿// Copyright (c) 2019 All Rights Reserved
// Dmitriy Annenkov
// 03 18 2019

using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ProBuilder;
using Weyland;
using Weyland.Builder;
using static UnityEngine.UI.Dropdown;
using Debug = UnityEngine.Debug;
using BuilderMesh = UnityEngine.ProBuilder.ProBuilderMesh;
using Weyland.Serializations;
using Weyland.Languages;

namespace Weyland.Builder
{
    public class GenerateTorus : GenerateMeshBehaviour
    {
        private PivotLocation pivotType = PivotLocation.Center;
        private int rows = 16;
        private int columns = 24;
        private float innerRadius = 1;
        private float outerRadius = 0.3f;
        private bool smooth = true;
        private float horizontalCircumference = 360;
        private float verticalCircumference = 360;
        private bool manualUvs = false;

        public override void PreInit()
        {
            string _namespace = this.GetType().FullName;

            pivotType = PlayerData.Read(_namespace + ".pivotType", pivotType);
            rows = PlayerData.Read(_namespace + ".rows", rows);
            columns = PlayerData.Read(_namespace + ".columns", columns);
            innerRadius = PlayerData.Read(_namespace + ".innerRadius", innerRadius);
            outerRadius = PlayerData.Read(_namespace + ".outerRadius", outerRadius);
            smooth = PlayerData.Read(_namespace + ".smooth", smooth);
            horizontalCircumference = PlayerData.Read(_namespace + ".horizontalCircumference", horizontalCircumference);
            verticalCircumference = PlayerData.Read(_namespace + ".verticalCircumference", verticalCircumference);
            manualUvs = PlayerData.Read(_namespace + ".manualUvs", manualUvs);

            GenerateMenu(_namespace);
            MeshGenerate();
        }

        private void GenerateMenu(string _namespace)
        {

            BuilderConfigure.title = Language.Read(_namespace, "Torus");

            BuilderConfigure.GenerateEnum(
                 Language.Read("Weyland.Builder.pivotType"),
                 Language.Read("Weyland.Builder.pivotType.info"), typeof(PivotLocation), (int)pivotType).AddListener((int value) =>
                 {
                     pivotType = PlayerData.Write(_namespace + ".pivotLocation", (PivotLocation)value);
                     MeshGenerate();
                 });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".rows", "Rows"),
                Language.Read(_namespace + ".rows.info", "The number of horizontal divisions.<"), rows).AddListener((int value) =>
                {
                    rows = PlayerData.Write(_namespace + ".rows", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".columns", "Columns"),
                Language.Read(_namespace + ".columns.info", "The number of vertical divisions."), columns).AddListener((int value) =>
                {
                    columns = PlayerData.Write(_namespace + ".columns", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".innerRadius", "Inner Radius"),
                Language.Read(_namespace + ".innerRadius.info", "The distance from center to the inner bound of geometry"), innerRadius).AddListener((float value) =>
                {
                    innerRadius = PlayerData.Write(_namespace + ".innerRadius", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".outerRadius", "Outer Radius"),
                Language.Read(_namespace + ".outerRadius.info", "The distance from center to the outer bound of geometry."), outerRadius).AddListener((float value) =>
                {
                    outerRadius = PlayerData.Write(_namespace + ".outerRadius", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".smooth", "Smooth"),
                Language.Read(_namespace + ".smooth.info", "True marks all faces as one smoothing group, false does not."), smooth).AddListener((bool value) =>
                {
                    smooth = PlayerData.Write(_namespace + ".smooth", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".horizontalCircumference", "Horizontal Circumference"),
                Language.Read(_namespace + ".horizontalCircumference.info", "The circumference of the horizontal in degrees."), horizontalCircumference).AddListener((float value) =>
                {
                    horizontalCircumference = PlayerData.Write(_namespace + ".horizontalCircumference", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".verticalCircumference", "Vertical Circumference"),
                Language.Read(_namespace + ".verticalCircumference.info", "The circumference of the vertical geometry in degrees."), verticalCircumference).AddListener((float value) =>
                {
                    verticalCircumference = PlayerData.Write(_namespace + ".verticalCircumference", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".manualUvs", "Manual UV"),
                Language.Read(_namespace + ".manualUvs.info", "A torus shape does not unwrap textures well using automatic UVs. To disable this feature and instead use manual UVs, pass true."), manualUvs).AddListener((bool value) =>
                {
                    manualUvs = PlayerData.Write(_namespace + ".manualUvs", value);
                    MeshGenerate();
                });
        }
        private void MeshGenerate() => Generate(ShapeGenerator.GenerateTorus(pivotType, rows, columns, innerRadius, outerRadius, smooth, horizontalCircumference, verticalCircumference, manualUvs));
    }
}