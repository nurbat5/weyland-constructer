﻿// Copyright (c) 2019 All Rights Reserved
// Dmitriy Annenkov
// 03 18 2019

using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ProBuilder;
using Weyland;
using Weyland.Builder;
using static UnityEngine.UI.Dropdown;
using Debug = UnityEngine.Debug;
using BuilderMesh = UnityEngine.ProBuilder.ProBuilderMesh;
using Weyland.Serializations;
using Weyland.Languages;

namespace Weyland.Builder
{
    public class GeneratePrism : GenerateMeshBehaviour
    {
        private PivotLocation pivotType = PivotLocation.Center;
        private float3 size = Vector3.one;

        public override void PreInit()
        {
            string _namespace = this.GetType().FullName;

            pivotType = PlayerData.Read(_namespace + ".pivotType", pivotType);
            size = PlayerData.Read(_namespace + ".size", size);

            GenerateMenu(_namespace);
            MeshGenerate();
        }

        private void GenerateMenu(string _namespace)
        {
            BuilderConfigure.title = Language.Read(_namespace, "Prism");

            BuilderConfigure.GenerateEnum(
                 Language.Read("Weyland.Builder.pivotType"),
                 Language.Read("Weyland.Builder.pivotType.info"), typeof(PivotLocation), (int)pivotType).AddListener((int value) =>
                {
                    pivotType = PlayerData.Write(_namespace + ".pivotLocation", (PivotLocation)value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".size", "Size"),
                Language.Read(_namespace + ".size.Info", "Scale to apply to the shape."), size).AddListener((float3 value) =>
                {
                    size = PlayerData.Write(_namespace + ".size", value);
                    MeshGenerate();
                });


        }
        private void MeshGenerate() => Generate(ShapeGenerator.GeneratePrism(pivotType, size));
    }
}