﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Weyland.InputSystem;
using Weyland.Serializations;

namespace Weyland
{
    public class BuilderConfigure : MonoBehaviour
    {
        public enum BuilderConfigureEnum
        {
            @int,
            @string,
            @float,
            @float2,
            @float3,
            @float4,
            @bool,
            Image,
            Dropdown,
            Button,
            Info
        }
        [Serializable]
        public struct BuilderConfigureAsset
        {
            public string name;
            public BuilderConfigureEnum type;
            public GameObject prefab;
        }
        // Property
        public static BuilderConfigure instance { get; private set; }
        public class BuilderEvent<T> : UnityEvent<T> { }
        public Transform content;
        public Text headerText;
        public List<BuilderConfigureAsset> asset; //Assets

        // Method public
        public static string title
        {
            get => instance.headerText.text;
            set => instance.headerText.text = HandleAlignment.textMessage = value;
        }
        public static UnityEvent<int> GenerateEnum(string title, string info, Type enumType, int defaultValue = 0)
        {
            Configure(BuilderConfigureEnum.Dropdown);
            Configure<Text>("Title").text = title;
            Configure<Text>("Info").text = info;
            Configure<Dropdown>().options.Clear();
            foreach(var option in Enum.GetNames(enumType))
                Configure<Dropdown>().options.Add(new Dropdown.OptionData { text = option });
            Configure<Dropdown>().value = defaultValue;
            return Configure<Dropdown>().onValueChanged;
        }
        public static UnityEvent GenerateButton(string buttonName)
        {
            Configure(BuilderConfigureEnum.Button);
            Configure<Text>().text = buttonName;
            
            return Configure<Button>().onClick;
        }
        
        public class ImageEvent<T, TextureComponent> : UnityEvent<T, TextureComponent> { }
        public static ImageEvent<int, TextureDecode> GenerateImage(string title, string info, TextureDecode[] textures, int defaultValue = 0, Texture2D texture = null)
        {
            ImageEvent<int, TextureDecode> builderEvent = new ImageEvent<int, TextureDecode>();
            Configure(BuilderConfigureEnum.Image);
            Configure<Text>("Title").text = title;
            Configure<Text>("Info").text = info;
            Configure<Dropdown>().options.Clear();
            foreach(var tex in textures)
                Configure<Dropdown>().options.Add(new Dropdown.OptionData { text = tex.name });
            Configure<Dropdown>().value = defaultValue;
            var rawImage = Configure<RawImage>();
            rawImage.texture = texture;
            Configure<Dropdown>().onValueChanged.AddListener(delegate(int call) 
            {
                rawImage.texture = textures[call].GenerateTexture();
                builderEvent.Invoke(call, textures[call]);
            });
            return builderEvent;
        }

        public static BuilderEvent<T> Generate<T>(string title, string info, T defaultValue)
        {
            BuilderEvent<T> builderEvent = new BuilderEvent<T>();
            if (typeof(T) == typeof(int)) return Int(title, info, defaultValue, builderEvent);
            else if (typeof(T) == typeof(string)) return String(title, info, defaultValue, builderEvent);
            else if (typeof(T) == typeof(float)) return Float(title, info, defaultValue, builderEvent);
            else if (typeof(T) == typeof(float2)) return Float2(title, info, defaultValue, builderEvent);
            else if (typeof(T) == typeof(float3)) return Float3(title, info, defaultValue, builderEvent);
            else if (typeof(T) == typeof(float4)) return Float4(title, info, defaultValue, builderEvent);
            else if (typeof(T) == typeof(bool)) return Bool(title, info, defaultValue, builderEvent);
            else if (typeof(T) == typeof(object)) return Info(title, builderEvent);
            return Info<T>(builderEvent);
        }
        public static void Clear()
        {
            title = null;
            for (int i = 0; i < instance.content.childCount; i++)
                GameObject.Destroy(instance.content.GetChild(i).gameObject);
        }

        private bool _isShow;
        public static bool showWindow
        {
            get => instance._isShow;
            set
            {
                instance._isShow = value;
                instance.transform.GetChild(0).gameObject.SetActive(value);
                if (value) InputManager.Player.Disable();
                else InputManager.Player.Enable();
            }
        } 

        // BLOCK
        private static BuilderEvent<T> Info<T>(BuilderEvent<T> builderEvent)
        {
            Configure(BuilderConfigureEnum.Info);
            Configure<Text>().text = $"[{typeof(T)}]";
            return builderEvent;
        }
        private static BuilderEvent<T> Float4<T>(string title, string info, T defaultValue, BuilderEvent<T> builderEvent)
        {
            Configure(BuilderConfigureEnum.@float4);
            float4 vector = (float4)(object)defaultValue;
            var inputs = ConfigureGets<InputField>("Text");
            Configure<Text>("Title").text = title;
            Configure<Text>("Info").text = info;

            inputs[0].text = vector.x.ToString();
            inputs[0].onValueChanged.AddListener((string text) =>
            {
                float parse = 0;
                float.TryParse(text, out parse);
                vector.x = parse;
                builderEvent.Invoke((T)Convert.ChangeType(vector, typeof(T)));
            });

            inputs[1].text = vector.y.ToString();
            inputs[1].onValueChanged.AddListener((string text) =>
            {
                float parse = 0;
                float.TryParse(text, out parse);
                vector.y = parse;
                builderEvent.Invoke((T)Convert.ChangeType(vector, typeof(T)));
            });

            inputs[2].text = vector.z.ToString();
            inputs[2].onValueChanged.AddListener((string text) =>
            {
                float parse = 0;
                float.TryParse(text, out parse);
                vector.z = parse;
                builderEvent.Invoke((T)Convert.ChangeType(vector, typeof(T)));
            });

            inputs[3].text = vector.z.ToString();
            inputs[3].onValueChanged.AddListener((string text) =>
            {
                float parse = 0;
                float.TryParse(text, out parse);
                vector.w = parse;
                builderEvent.Invoke((T)Convert.ChangeType(vector, typeof(T)));
            });
            return builderEvent;
        }
        private static BuilderEvent<T> Float3<T>(string title, string info, T defaultValue, BuilderEvent<T> builderEvent)
        {
            // BuilderEvent<T> float3Event = new BuilderEvent<T>();
            Configure(BuilderConfigureEnum.@float3);
            float3 vector = (float3)(object)defaultValue;
            var inputs = ConfigureGets<InputField>("Text");
            Configure<Text>("Title").text = title;
            Configure<Text>("Info").text = info;

            inputs[0].text = vector.x.ToString();
            inputs[0].onValueChanged.AddListener((string text) =>
            {
                float parse = 0;
                float.TryParse(text, out parse);
                vector.x = parse;
                builderEvent.Invoke((T)Convert.ChangeType(vector, typeof(T)));
            });

            inputs[1].text = vector.y.ToString();
            inputs[1].onValueChanged.AddListener((string text) =>
            {
                float parse = 0;
                float.TryParse(text, out parse);
                vector.y = parse;
                builderEvent.Invoke((T)Convert.ChangeType(vector, typeof(T)));
            });

            inputs[2].text = vector.z.ToString();
            inputs[2].onValueChanged.AddListener((string text) =>
            {
                float parse = 0;
                float.TryParse(text, out parse);
                vector.z = parse;
                builderEvent.Invoke((T)Convert.ChangeType(vector, typeof(T)));
            });
            return builderEvent;
        }
        private static BuilderEvent<T> Float2<T>(string title, string info, T defaultValue, BuilderEvent<T> builderEvent)
        {
            // BuilderEvent<T> float3Event = new BuilderEvent<T>();
            Configure(BuilderConfigureEnum.@float2);
            float2 vector = (float2)(object)defaultValue;
            var inputs = ConfigureGets<InputField>("Text");
            Configure<Text>("Title").text = title;
            Configure<Text>("Info").text = info;

            inputs[0].text = vector.x.ToString();
            inputs[0].onValueChanged.AddListener((string text) =>
            {
                float parse = 0;
                float.TryParse(text, out parse);
                vector.x = parse;
                builderEvent.Invoke((T)Convert.ChangeType(vector, typeof(T)));
            });

            inputs[1].text = vector.y.ToString();
            inputs[1].onValueChanged.AddListener((string text) =>
            {
                float parse = 0;
                float.TryParse(text, out parse);
                vector.y = parse;
                builderEvent.Invoke((T)Convert.ChangeType(vector, typeof(T)));
            });

            return builderEvent;
        }
        private static BuilderEvent<T> Float<T>(string title, string info, T defaultValue, BuilderEvent<T> builderEvent)
        {
            Configure(BuilderConfigureEnum.@float);
            Configure<Text>("Title").text = title;
            Configure<Text>("Info").text = info;
            Configure<InputField>().text = defaultValue.ToString();
            Configure<InputField>().onValueChanged.AddListener((string text) =>
            {
                float parse = 0;
                float.TryParse(text, out parse);
                builderEvent.Invoke((T)Convert.ChangeType(parse, typeof(T)));
            });
            return builderEvent;
        }
        private static BuilderEvent<T> String<T>(string title, string info, T defaultValue, BuilderEvent<T> builderEvent)
        {
            Configure(BuilderConfigureEnum.@string);
            Configure<Text>("Title").text = title;
            Configure<Text>("Info").text = info;
            Configure<InputField>().text = (string)(object)defaultValue;
            Configure<InputField>().onValueChanged.AddListener((string text) =>
            {
                builderEvent.Invoke((T)Convert.ChangeType(text, typeof(T)));
            });
            return builderEvent;
        }
        private static BuilderEvent<T> Int<T>(string title, string info, T defaultValue, BuilderEvent<T> builderEvent)
        {
            Configure(BuilderConfigureEnum.@int);
            Configure<Text>("Title").text = title;
            Configure<Text>("Info").text = info;
            if (title == null) Destroy(Configure<Text>("Title"));
            if (info == null) Destroy(Configure<Text>("Info"));
            Configure<InputField>().text = defaultValue.ToString();
            Configure<InputField>().onValueChanged.AddListener((string text) =>
            {
                int parse = 0;
                int.TryParse(text, out parse);
                builderEvent.Invoke((T)Convert.ChangeType(parse, typeof(T)));
            });
            return builderEvent;
        }
        private static BuilderEvent<T> Bool<T>(string title, string info, T defaultValue, BuilderEvent<T> builderEvent)
        {
            Configure(BuilderConfigureEnum.@bool);
            Configure<Text>("Title").text = title;
            Configure<Text>("Info").text = info;
            if (title == null) Destroy(Configure<Text>("Title"));
            if (info == null) Destroy(Configure<Text>("Info"));
            // Configure<RawImage>().gameObject.SetActive();
            Configure<Toggle>().isOn = (bool)(object)defaultValue;
            Configure<Toggle>().onValueChanged.AddListener((bool call) =>
            {
                builderEvent.Invoke((T)Convert.ChangeType(call, typeof(T)));
            });
            return builderEvent;
        }
        private static BuilderEvent<T> Info<T>(string title, BuilderEvent<T> builderEvent)
        {
            Configure(BuilderConfigureEnum.Info);
            Configure<Text>().text = title;
            return builderEvent;
        }

        //CONFIGURE
        private static void Configure(BuilderConfigureEnum type) 
            => instance._cacheConfigure = Instantiate(instance.asset.Find(u => u.type == type).prefab, instance.content).transform;

        private static T Configure<T>(string parentName) => instance._cacheConfigure.Find(parentName).GetComponent<T>();
        private static T Configure<T>() => instance._cacheConfigure.GetComponentInChildren<T>();
        private static T[] ConfigureGets<T>() => instance._cacheConfigure.GetComponentsInChildren<T>();
        private static T[] ConfigureGets<T>(string parentName) => instance._cacheConfigure.Find(parentName).GetComponentsInChildren<T>();
        private static T ConfigureGet<T>() => instance._cacheConfigure.GetComponent<T>();

        private void Update() { if(Keyboard.current.tKey.wasReleasedThisFrame) WindowGame.Open(WindowGame.TypeWindow.ToolsSettings); }
        private void Awake() => instance = this;
        private Transform _cacheConfigure;
    }
}