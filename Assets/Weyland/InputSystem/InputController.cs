// GENERATED AUTOMATICALLY FROM 'Assets/New Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace Weyland.InputSystem
{
    public class InputController : IInputActionCollection
    {
        private InputActionAsset asset;
        public InputController()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""New Controls"",
    ""maps"": [
        {
            ""name"": ""Toolbar"",
            ""id"": ""7ab10824-73fd-4177-9436-fbb1ccbeca50"",
            ""actions"": [
                {
                    ""name"": ""digitalKey"",
                    ""id"": ""fc6edada-ed1d-451f-a34b-7932cc59397c"",
                    ""expectedControlLayout"": ""Axis"",
                    ""continuous"": false,
                    ""passThrough"": true,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": ""Press"",
                    ""bindings"": []
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""1a7274f5-2cdf-4673-b74e-9624f8ea279c"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": ""Scale"",
                    ""groups"": """",
                    ""action"": ""digitalKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""fddf31f5-2583-45da-b1d1-30ec03bc13b3"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": ""Scale(factor=2)"",
                    ""groups"": """",
                    ""action"": ""digitalKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""58128b78-7587-4431-8eb9-3bca1c8214c2"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": ""Scale(factor=3)"",
                    ""groups"": """",
                    ""action"": ""digitalKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""a55f2c23-ffbc-4320-acd6-627042b9b4f7"",
                    ""path"": ""<Keyboard>/4"",
                    ""interactions"": """",
                    ""processors"": ""Scale(factor=4)"",
                    ""groups"": """",
                    ""action"": ""digitalKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""5a96a95e-59b7-4548-ad94-910866d83d5d"",
                    ""path"": ""<Keyboard>/5"",
                    ""interactions"": """",
                    ""processors"": ""Scale(factor=5)"",
                    ""groups"": """",
                    ""action"": ""digitalKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""06092b86-6982-4494-a1cd-8d7db132033e"",
                    ""path"": ""<Keyboard>/6"",
                    ""interactions"": """",
                    ""processors"": ""Scale(factor=6)"",
                    ""groups"": """",
                    ""action"": ""digitalKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""44cad719-dbb6-4b8a-85cb-5b8658a312ec"",
                    ""path"": ""<Keyboard>/7"",
                    ""interactions"": """",
                    ""processors"": ""Scale(factor=7)"",
                    ""groups"": """",
                    ""action"": ""digitalKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""ea8efdac-4cf9-4ce0-86ff-6423a4fc39f6"",
                    ""path"": ""<Keyboard>/8"",
                    ""interactions"": """",
                    ""processors"": ""Scale(factor=8)"",
                    ""groups"": """",
                    ""action"": ""digitalKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""fe3f6680-b4ee-4ee8-981f-4ad6f9891379"",
                    ""path"": ""<Keyboard>/9"",
                    ""interactions"": """",
                    ""processors"": ""Scale(factor=9)"",
                    ""groups"": """",
                    ""action"": ""digitalKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""ae45c037-06c3-435c-9ea0-0faa34ac8d8b"",
                    ""path"": ""<Keyboard>/0"",
                    ""interactions"": """",
                    ""processors"": ""Scale(factor=0)"",
                    ""groups"": """",
                    ""action"": ""digitalKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""c3755831-ee52-453f-a500-a9fc9b00cbfa"",
                    ""path"": ""<Keyboard>/#(`)"",
                    ""interactions"": """",
                    ""processors"": ""Scale(factor=10)"",
                    ""groups"": """",
                    ""action"": ""digitalKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                }
            ]
        },
        {
            ""name"": ""Player"",
            ""id"": ""78f63de8-ddf2-4d09-ad96-1917ee14c443"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""id"": ""a68c02fd-ae37-402b-abce-040d6affe3d7"",
                    ""expectedControlLayout"": """",
                    ""continuous"": false,
                    ""passThrough"": true,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""Sprint"",
                    ""id"": ""133780a2-fc00-4cad-b5c1-59e565e156ce"",
                    ""expectedControlLayout"": """",
                    ""continuous"": false,
                    ""passThrough"": true,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""Jump"",
                    ""id"": ""8a2ad45c-a988-4ca8-8238-f98207aae772"",
                    ""expectedControlLayout"": """",
                    ""continuous"": true,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": ""MultiTap"",
                    ""bindings"": []
                },
                {
                    ""name"": ""MouseDelta"",
                    ""id"": ""2e72a1c3-84ab-4bcd-88bd-79d2946870f0"",
                    ""expectedControlLayout"": """",
                    ""continuous"": true,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": ""MultiTap"",
                    ""bindings"": []
                },
                {
                    ""name"": ""MouseScroll"",
                    ""id"": ""e561cf99-7ebc-48e8-b8ba-1ddab1c94c27"",
                    ""expectedControlLayout"": """",
                    ""continuous"": true,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": ""MultiTap"",
                    ""bindings"": []
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""a93f9e55-e996-48c2-9a83-9df665d78a79"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": ""up"",
                    ""id"": ""b7503482-59fe-4f42-bc79-2e73d55422bf"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true,
                    ""modifiers"": """"
                },
                {
                    ""name"": ""down"",
                    ""id"": ""fc3adfad-f5c4-4c2e-bff2-851f9ccb5354"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true,
                    ""modifiers"": """"
                },
                {
                    ""name"": ""left"",
                    ""id"": ""e91383ad-5d15-4aaf-a81b-01ac3b0ca845"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true,
                    ""modifiers"": """"
                },
                {
                    ""name"": ""right"",
                    ""id"": ""1d62e14d-fd57-44b4-8454-656c8b325f1b"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""9163102e-b609-4713-bc00-37d6c975d48e"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Sprint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""f2f23e9a-a405-48ae-be64-d3b11de0cc29"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""6a9bf4a0-a9a6-48f2-83f8-3f5093e4fb54"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseDelta"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""43163e22-02b6-4a99-b53b-d16a4bfe14d7"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseScroll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                }
            ]
        },
        {
            ""name"": ""Menus"",
            ""id"": ""c365a0af-aca2-4206-ba55-8bacac247b2e"",
            ""actions"": [
                {
                    ""name"": ""TMenu"",
                    ""id"": ""a9e51bb8-1032-4c2b-b06e-109a588cfe2c"",
                    ""expectedControlLayout"": """",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""ESC"",
                    ""id"": ""10fcdd3a-5ce8-4c59-88f4-5aedc01199d2"",
                    ""expectedControlLayout"": """",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""8ef98feb-8c47-4610-8be6-f0d1c06cdc2c"",
                    ""path"": ""<Keyboard>/t"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""ea6ba067-7ee8-4a6f-8693-f093e3cbf1bf"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ESC"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""DefaultControl"",
            ""basedOn"": """",
            ""bindingGroup"": ""DefaultControl"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
            // Toolbar
            m_Toolbar = asset.GetActionMap("Toolbar");
            m_Toolbar_digitalKey = m_Toolbar.GetAction("digitalKey");
            // Player
            m_Player = asset.GetActionMap("Player");
            m_Player_Move = m_Player.GetAction("Move");
            m_Player_Sprint = m_Player.GetAction("Sprint");
            m_Player_Jump = m_Player.GetAction("Jump");
            m_Player_MouseDelta = m_Player.GetAction("MouseDelta");
            m_Player_MouseScroll = m_Player.GetAction("MouseScroll");
            // Menus
            m_Menus = asset.GetActionMap("Menus");
            m_Menus_TMenu = m_Menus.GetAction("TMenu");
            m_Menus_ESC = m_Menus.GetAction("ESC");
        }

        ~InputController()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes
        {
            get => asset.controlSchemes;
        }

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Toolbar
        private InputActionMap m_Toolbar;
        private IToolbarActions m_ToolbarActionsCallbackInterface;
        private InputAction m_Toolbar_digitalKey;
        public struct ToolbarActions
        {
            private InputController m_Wrapper;
            public ToolbarActions(InputController wrapper) { m_Wrapper = wrapper; }
            public InputAction @digitalKey { get { return m_Wrapper.m_Toolbar_digitalKey; } }
            public InputActionMap Get() { return m_Wrapper.m_Toolbar; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled { get { return Get().enabled; } }
            public InputActionMap Clone() { return Get().Clone(); }
            public static implicit operator InputActionMap(ToolbarActions set) { return set.Get(); }
            public void SetCallbacks(IToolbarActions instance)
            {
                if (m_Wrapper.m_ToolbarActionsCallbackInterface != null)
                {
                    digitalKey.started -= m_Wrapper.m_ToolbarActionsCallbackInterface.OnDigitalKey;
                    digitalKey.performed -= m_Wrapper.m_ToolbarActionsCallbackInterface.OnDigitalKey;
                    digitalKey.canceled -= m_Wrapper.m_ToolbarActionsCallbackInterface.OnDigitalKey;
                }
                m_Wrapper.m_ToolbarActionsCallbackInterface = instance;
                if (instance != null)
                {
                    digitalKey.started += instance.OnDigitalKey;
                    digitalKey.performed += instance.OnDigitalKey;
                    digitalKey.canceled += instance.OnDigitalKey;
                }
            }
        }
        public ToolbarActions @Toolbar
        {
            get
            {
                return new ToolbarActions(this);
            }
        }

        // Player
        private InputActionMap m_Player;
        private IPlayerActions m_PlayerActionsCallbackInterface;
        private InputAction m_Player_Move;
        private InputAction m_Player_Sprint;
        private InputAction m_Player_Jump;
        private InputAction m_Player_MouseDelta;
        private InputAction m_Player_MouseScroll;
        public struct PlayerActions
        {
            private InputController m_Wrapper;
            public PlayerActions(InputController wrapper) { m_Wrapper = wrapper; }
            public InputAction @Move { get { return m_Wrapper.m_Player_Move; } }
            public InputAction @Sprint { get { return m_Wrapper.m_Player_Sprint; } }
            public InputAction @Jump { get { return m_Wrapper.m_Player_Jump; } }
            public InputAction @MouseDelta { get { return m_Wrapper.m_Player_MouseDelta; } }
            public InputAction @MouseScroll { get { return m_Wrapper.m_Player_MouseScroll; } }
            public InputActionMap Get() { return m_Wrapper.m_Player; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled { get { return Get().enabled; } }
            public InputActionMap Clone() { return Get().Clone(); }
            public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
            public void SetCallbacks(IPlayerActions instance)
            {
                if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
                {
                    Move.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                    Move.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                    Move.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                    Sprint.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSprint;
                    Sprint.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSprint;
                    Sprint.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSprint;
                    Jump.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                    Jump.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                    Jump.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                    MouseDelta.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMouseDelta;
                    MouseDelta.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMouseDelta;
                    MouseDelta.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMouseDelta;
                    MouseScroll.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMouseScroll;
                    MouseScroll.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMouseScroll;
                    MouseScroll.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMouseScroll;
                }
                m_Wrapper.m_PlayerActionsCallbackInterface = instance;
                if (instance != null)
                {
                    Move.started += instance.OnMove;
                    Move.performed += instance.OnMove;
                    Move.canceled += instance.OnMove;
                    Sprint.started += instance.OnSprint;
                    Sprint.performed += instance.OnSprint;
                    Sprint.canceled += instance.OnSprint;
                    Jump.started += instance.OnJump;
                    Jump.performed += instance.OnJump;
                    Jump.canceled += instance.OnJump;
                    MouseDelta.started += instance.OnMouseDelta;
                    MouseDelta.performed += instance.OnMouseDelta;
                    MouseDelta.canceled += instance.OnMouseDelta;
                    MouseScroll.started += instance.OnMouseScroll;
                    MouseScroll.performed += instance.OnMouseScroll;
                    MouseScroll.canceled += instance.OnMouseScroll;
                }
            }
        }
        public PlayerActions @Player
        {
            get
            {
                return new PlayerActions(this);
            }
        }

        // Menus
        private InputActionMap m_Menus;
        private IMenusActions m_MenusActionsCallbackInterface;
        private InputAction m_Menus_TMenu;
        private InputAction m_Menus_ESC;
        public struct MenusActions
        {
            private InputController m_Wrapper;
            public MenusActions(InputController wrapper) { m_Wrapper = wrapper; }
            public InputAction @TMenu { get { return m_Wrapper.m_Menus_TMenu; } }
            public InputAction @ESC { get { return m_Wrapper.m_Menus_ESC; } }
            public InputActionMap Get() { return m_Wrapper.m_Menus; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled { get { return Get().enabled; } }
            public InputActionMap Clone() { return Get().Clone(); }
            public static implicit operator InputActionMap(MenusActions set) { return set.Get(); }
            public void SetCallbacks(IMenusActions instance)
            {
                if (m_Wrapper.m_MenusActionsCallbackInterface != null)
                {
                    TMenu.started -= m_Wrapper.m_MenusActionsCallbackInterface.OnTMenu;
                    TMenu.performed -= m_Wrapper.m_MenusActionsCallbackInterface.OnTMenu;
                    TMenu.canceled -= m_Wrapper.m_MenusActionsCallbackInterface.OnTMenu;
                    ESC.started -= m_Wrapper.m_MenusActionsCallbackInterface.OnESC;
                    ESC.performed -= m_Wrapper.m_MenusActionsCallbackInterface.OnESC;
                    ESC.canceled -= m_Wrapper.m_MenusActionsCallbackInterface.OnESC;
                }
                m_Wrapper.m_MenusActionsCallbackInterface = instance;
                if (instance != null)
                {
                    TMenu.started += instance.OnTMenu;
                    TMenu.performed += instance.OnTMenu;
                    TMenu.canceled += instance.OnTMenu;
                    ESC.started += instance.OnESC;
                    ESC.performed += instance.OnESC;
                    ESC.canceled += instance.OnESC;
                }
            }
        }
        public MenusActions @Menus
        {
            get
            {
                return new MenusActions(this);
            }
        }
        private int m_DefaultControlSchemeIndex = -1;
        public InputControlScheme DefaultControlScheme
        {
            get
            {
                if (m_DefaultControlSchemeIndex == -1) m_DefaultControlSchemeIndex = asset.GetControlSchemeIndex("DefaultControl");
                return asset.controlSchemes[m_DefaultControlSchemeIndex];
            }
        }
        public interface IToolbarActions
        {
            void OnDigitalKey(InputAction.CallbackContext context);
        }
        public interface IPlayerActions
        {
            void OnMove(InputAction.CallbackContext context);
            void OnSprint(InputAction.CallbackContext context);
            void OnJump(InputAction.CallbackContext context);
            void OnMouseDelta(InputAction.CallbackContext context);
            void OnMouseScroll(InputAction.CallbackContext context);
        }
        public interface IMenusActions
        {
            void OnTMenu(InputAction.CallbackContext context);
            void OnESC(InputAction.CallbackContext context);
        }
    }
}
