using System;
using UnityEngine;
using UnityEngine.UI;
using Weyland.InventorySystem;

namespace Weyland
{
    [Serializable]
    public struct ToolbarElement
    {
        public GameObject activeObject;
        public RawImage imageObject;
        public InventoryElementAsset inventoryElement;
    }
}