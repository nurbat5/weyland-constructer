using System;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace Weyland.Serializations
{
    [Serializable]
    public struct TextureDecode
    {
        public string name;
        public TextureFormat textureFormat;
        public int width;
        public int height;
        public bool isReadable;
        public byte[] byteTexture;

        public Texture2D GenerateTexture()
        {
            if(byteTexture == null) return new Texture2D(1,1);
            Texture2D texture = new Texture2D(width, height, textureFormat, false, true);
            if (isReadable)
            {
                texture.LoadImage(byteTexture);
                return texture;
            }

            texture.LoadRawTextureData(byteTexture);
            texture.Apply();
            return texture;
        }

        public static TextureDecode Create()
        {
            return new TextureDecode
            {
                name = null,
                byteTexture = null,
                width = 0,
                height = 0,
                textureFormat = TextureFormat.RGBA32
            };
        }
        public static TextureDecode Create(Texture texture) => Create(texture as Texture2D);
        public static TextureDecode Create(Texture2D texture)
        {
            if(texture == null) return Create();
            return new TextureDecode
            {
                name = texture.name,
                width = texture.width,
                height = texture.height,
                textureFormat = texture.format,
                isReadable = false,//texture.isReadable,
                byteTexture = texture.GetRawTextureData()//texture.isReadable ? texture.EncodeToPNG() : texture.GetRawTextureData()
            };
        }
        public static TextureDecode CreatePNG(byte[] textureByte)
        {
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(textureByte);
            return new TextureDecode
            {
                name = texture.name,
                width = texture.width,
                height = texture.height,
                textureFormat = texture.format,
                isReadable = true,
                byteTexture = textureByte
            };
        }
    }
}