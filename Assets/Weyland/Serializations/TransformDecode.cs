using System;
using Unity.Mathematics;
using UnityEngine;

namespace Weyland.Serializations
{
    [Serializable]
    public struct TransformDecode
    {
        public float3 position;
        public float3 rotation;

        public TransformDecode(GameObject gameObject) : this(gameObject.transform) { }
        public TransformDecode(Transform transform)
        {
            this.position = transform.position;
            this.rotation = transform.eulerAngles;
        }
        public TransformDecode(string json) => this = JsonUtility.FromJson<TransformDecode>(json);
        public string ToJson() => JsonUtility.ToJson(this, true);
        public void Apply(GameObject gameObject) => Apply(gameObject.transform);
        public void Apply(Transform transform)
        {
            transform.position = position;
            transform.rotation = Quaternion.Euler(rotation);
        }
    }
}