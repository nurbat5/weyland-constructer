﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TestLoadImage : MonoBehaviour
{
    public Texture2D tex;
    private void Start()
    {
        if (File.Exists($"{Application.dataPath}/Textures/test.png"))
        {
            using (FileStream fstream = File.OpenRead($"{Application.dataPath}/Textures/test.png"))
            {
                byte[] array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                Texture2D text = new Texture2D(1, 1);
                text.LoadImage(array);
                tex = text;
            }
            Debug.Log("PK");
            return;
        }
            Debug.Log("errr");
    }
}
