﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoQuality : BaseQualitySettings
    {
        public int defaultQuality;
        public int quality 
        { 
            get => PlayerData.Read(OptionsKey.Quality, defaultQuality); 
            set => PlayerData.Write(OptionsKey.Quality, value); 
        }


        public void OnDisable() => QualitySettings.SetQualityLevel(quality);
        
        public override void Init()
        {
            nameText = Language.Read(OptionsKey.Quality, "Profile Quality");
            valueText = QualitySettings.names[quality].ToUpper();
        }
        public override void Prev() => SetQuality(false);
        public override void Next() => SetQuality(true);

        public void SetQuality(bool mas)
        {
            if (mas)quality = (quality + 1) % QualitySettings.names.Length;
            else
            {
                if (quality != 0) quality = (quality - 1) % QualitySettings.names.Length;
                else quality = (QualitySettings.names.Length - 1);
            }
            valueText = QualitySettings.names[quality].ToUpper();
            QualitySettings.SetQualityLevel(quality);
        }
    }
}
