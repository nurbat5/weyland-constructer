using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoTextureLimit : BaseQualitySettings
    {
        public int textureLimit
        {
            get => PlayerData.Read(OptionsKey.TextureLimit, 0);
            set => PlayerData.Write(OptionsKey.TextureLimit, value);
        }

        public override void PreInit()
        {
            nameText = Language.Read(OptionsKey.TextureLimit, "Texture Quality");
            QualitySettings.masterTextureLimit = textureLimit;
            valueText = TextureQualityNames[textureLimit];
        }

        public override void Prev() => SetTextureQuality(false);
        public override void Next() => SetTextureQuality(true);

        public void SetTextureQuality(bool mas)
        {
            if(mas) textureLimit = (textureLimit + 1) % 3;
            else if(textureLimit != 0) textureLimit = (textureLimit - 1) % 3;
            else textureLimit = 3;

            QualitySettings.masterTextureLimit = textureLimit;
            valueText = TextureQualityNames[textureLimit];
        }
        private string[] TextureQualityNames = new string[] { "FULL RES", "HALF RES", "QUARTER RES", "EIGHTH RES", };
    }
}
