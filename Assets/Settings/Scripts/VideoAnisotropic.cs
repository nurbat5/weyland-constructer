using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoAnisotropic : BaseQualitySettings
    {
        [Range(0, 2)]
        public int defaultAnisoTropic = 1;
        public int anisotropic
        {
            get => PlayerData.Read(OptionsKey.AnisoTropic, defaultAnisoTropic);
            set => PlayerData.Write(OptionsKey.AnisoTropic, value);
        }

        public override void PreInit()
        {
            nameText = Language.Read(OptionsKey.AnisoTropic, "Anisotropic Texture");
            ChangeAnisotropic();
        }
        public override void Prev() => SetAntiStropic(false);
        public override void Next () => SetAntiStropic(true);

        public void SetAntiStropic(bool mas)
        {
            if (mas) anisotropic = (anisotropic + 1) % 3;
            else if (anisotropic != 0) anisotropic = (anisotropic - 1) % 3;
            else anisotropic = 2;
            ChangeAnisotropic();
        }

        private void ChangeAnisotropic()
        {
            switch (anisotropic)
            {
                case 0:
                    QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
                    valueText = AnisotropicFiltering.Disable.ToString().ToUpper();
                    break;
                case 1:
                    QualitySettings.anisotropicFiltering = AnisotropicFiltering.Enable;
                    valueText = AnisotropicFiltering.Enable.ToString().ToUpper();
                    break;
                case 2:
                    QualitySettings.anisotropicFiltering = AnisotropicFiltering.ForceEnable;
                    valueText = AnisotropicFiltering.ForceEnable.ToString().ToUpper();
                    break;
            }
        }
    }
}
