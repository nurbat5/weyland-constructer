﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class GamePlayerPosition : BaseQualitySettings
    {
        public bool savePlayerPosition 
        { 
            get => PlayerData.Read(OptionsKey.SavePlayerPosition, false); 
            set 
            {
                PlayerData.Write(OptionsKey.SavePlayerPosition, value);
                valueText = value ? "ON" : "OFF";
            }
        }

        public override void Init()
        {
            nameText = Language.Read(OptionsKey.SavePlayerPosition, "Save Player Position");
            valueText = savePlayerPosition ? "ON" : "OFF";
        }

        public override void Prev() => savePlayerPosition = false;
        public override void Next() => savePlayerPosition = true;
    }
}
