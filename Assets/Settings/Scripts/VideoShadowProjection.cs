using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoShadowProjection : BaseQualitySettings
    {
        public bool shadowProjection
        {
            get => PlayerData.Read(OptionsKey.ShadownProjection, true);
            set => PlayerData.Write(OptionsKey.ShadownProjection, value);
        }

        public override void PreInit()
        {
            nameText = Language.Read(OptionsKey.ShadownProjection, "Shadow Projection");
            SetShadowProjectionType(shadowProjection);
        }

        public override void Prev() => SetShadowProjectionType(false);
        public override void Next() => SetShadowProjectionType(true);

        public void SetShadowProjectionType(bool b)
        {
            if (shadowProjection = b)
            {
                QualitySettings.shadowProjection = ShadowProjection.StableFit;
                valueText = ShadowProjection.StableFit.ToString().ToUpper();
            }
            else
            {
                QualitySettings.shadowProjection = ShadowProjection.CloseFit;
                valueText = ShadowProjection.CloseFit.ToString().ToUpper();
            }
        }
    }
}
