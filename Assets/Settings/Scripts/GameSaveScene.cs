﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class GameSaveScene : BaseQualitySettings
    {
        public bool saveScene
        { 
            get => PlayerData.Read(OptionsKey.SaveScene, false); 
            set 
            {
                PlayerData.Write(OptionsKey.SaveScene, value);
                valueText = value ? "ON" : "OFF";
            }
        }

        public override void Init()
        {
            nameText = Language.Read(OptionsKey.SaveScene, "Save scene");
            valueText = saveScene ? "ON" : "OFF";
        }

        public override void Prev() => saveScene = false;
        public override void Next() => saveScene = true;
    }
}
