using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoAliasing : BaseQualitySettings
    {
        [Range(0, 3)]
        public int defaultAntiAliasing = 1;
        public int aliasing
        {
            get => PlayerData.Read(OptionsKey.AntiAliasing, defaultAntiAliasing);
            set => PlayerData.Write(OptionsKey.AntiAliasing, value);
        }

        public override void PreInit()
        {
            nameText = Language.Read(OptionsKey.AntiAliasing, "Anti Aliasing");
            ChangeAntiAliasing();
        }

        public override void Prev() => SetAntiAliasing(false);
        public override void Next () => SetAntiAliasing(true);

        public void SetAntiAliasing(bool mas)
        {
            if(mas) aliasing = (aliasing + 1) % 4;
            else if(aliasing != 0) aliasing = aliasing - 1 % 4;
            else aliasing = 3;
            ChangeAntiAliasing();
        }

        private void ChangeAntiAliasing()
        {
            valueText = AntiAliasingNames[aliasing].ToUpper();
            switch (aliasing)
            {
                case 0:
                    QualitySettings.antiAliasing = 0;
                    break;
                case 1:
                    QualitySettings.antiAliasing = 2;
                    break;
                case 2:
                    QualitySettings.antiAliasing = 4;
                    break;
                case 3:
                    QualitySettings.antiAliasing = 8;
                    break;
            }
        }
        private string[] AntiAliasingNames = new string[] { "X0", "X2", "X4", "X8" };
    }
}
