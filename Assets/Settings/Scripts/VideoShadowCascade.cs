using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoShadowCascade : BaseQualitySettings
    {
        public int shadowCascade
        {
            get => PlayerData.Read(OptionsKey.ShadowCascade, 0);
            set => PlayerData.Write(OptionsKey.ShadowCascade, value);
        }

        public override void PreInit()
        {
            nameText = Language.Read(OptionsKey.ShadowCascade, "Shadow Cascade");
            QualitySettings.shadowCascades = ShadowCascadeOptions[shadowCascade];
            valueText = ShadowCascadeNames[shadowCascade];
        }

        public override void Prev() => SetShadowCascades(false);
        public override void Next() => SetShadowCascades(true);

        public void SetShadowCascades(bool b)
        {
            if(b) shadowCascade = (shadowCascade + 1) % 3;
            else if(shadowCascade != 0 ) shadowCascade = (shadowCascade - 1) % 3;
            else shadowCascade = 3;

            QualitySettings.shadowCascades = ShadowCascadeOptions[shadowCascade];
            valueText = ShadowCascadeNames[shadowCascade];
        }
        private int[] ShadowCascadeOptions = new int[] { 0, 2, 4, 6 };
        private string[] ShadowCascadeNames = new string[] { "NO CASCADES", "TWO CASCADES", "FOUR CASCADES", "SIX CASCADES"};
    }
}
