using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoShadowDistance : MonoBehaviour
    {
        public float defaultShadowDistance = 40;
        public float shadowDistance
        {
            get => PlayerData.Read(OptionsKey.ShadowDistance, defaultShadowDistance);
            set 
            {
                PlayerData.Write(OptionsKey.ShadowDistance, value);
                if (PlayerData.Read(OptionsKey.ShadowEnable, false)) QualitySettings.shadowDistance = value;
                valueText = $"{value.ToString("F0")}m";
            distanceSlider.value = shadowDistance;
            }
        }

        private Text[] qualityText;
        private string nameText { get => qualityText[0].text; set => qualityText[0].text = value; }
        private string valueText { get => qualityText[1].text; set => qualityText[1].text = value; }
        private Slider distanceSlider;

        public void Awake()
        {
            qualityText = gameObject.GetComponentsInChildren<Text>();
            distanceSlider = gameObject.GetComponentInChildren<Slider>();
            distanceSlider.onValueChanged.AddListener((float value) => { shadowDistance = value; });
            distanceSlider.minValue = 0;
            distanceSlider.maxValue = 200;

            nameText = Language.Read(OptionsKey.ShadowDistance, "Shadow Distance");
            shadowDistance = shadowDistance;
        }
    }
}
