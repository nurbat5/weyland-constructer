using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoRealtimeReflection : BaseQualitySettings
    {
        public bool realtimeReflection
        {
            get => PlayerData.Read(OptionsKey.RealtimeReflection, true);
            set => PlayerData.Write(OptionsKey.RealtimeReflection, value);
        }

        public override void PreInit()
        {
            nameText = Language.Read(OptionsKey.RealtimeReflection, "Realtime Reflection");
            SetRealTimeReflection(realtimeReflection);
        }

        public override void Prev() => SetRealTimeReflection(false);
        public override void Next() => SetRealTimeReflection(true);

        public void SetRealTimeReflection(bool value)
        {
            QualitySettings.realtimeReflectionProbes = value;
            valueText = (realtimeReflection = value) ? "ENABLE" : "DISABLE";
        }
    }
}
