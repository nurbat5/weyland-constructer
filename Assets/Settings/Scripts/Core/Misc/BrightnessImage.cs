﻿using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class BrightnessImage : MonoBehaviour {

    private void Start() => transform.SetAsLastSibling();
    public void SetValue(float value) => GetComponent<CanvasGroup>().alpha = 1 - value;
}