using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoBrightness : MonoBehaviour
    {
        [Range(0, 1)]
        public float defaultBrightness = 1;
        public BrightnessImage brightnessImage;
        public float brightness
        {
            get => PlayerData.Read(OptionsKey.Brightness, defaultBrightness);
            set => PlayerData.Write(OptionsKey.Brightness, value);
        }

        private Text[] qualityText;
        private string nameText { get => qualityText[0].text; set => qualityText[0].text = value; }
        private string valueText { get => qualityText[1].text; set => qualityText[1].text = value; }
        private Slider distanceSlider;

        public void Awake()
        {
            qualityText = gameObject.GetComponentsInChildren<Text>();
            distanceSlider = gameObject.GetComponentInChildren<Slider>();
            distanceSlider.onValueChanged.AddListener((float value) => SetBrightness(value));
            distanceSlider.minValue = 0;
            distanceSlider.maxValue = 1;

            nameText = Language.Read(OptionsKey.Brightness, "Brightness");
            distanceSlider.value = brightness;
            SetBrightness(brightness);
        }

        public void SetBrightness(float value)
        {
            if (!brightnessImage) return;

            brightness = value;
            brightnessImage.SetValue(value);
            distanceSlider.value = value;
            valueText = $"{(value * 100).ToString("F0")}%";
        }
    }
}
