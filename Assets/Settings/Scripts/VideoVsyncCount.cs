using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoVsyncCount : BaseQualitySettings
    {
        [Range(0, 2)]
        public int defaultVSync = 1;
        public int vsyncCount
        {
            get => PlayerData.Read(OptionsKey.VsyncCount, defaultVSync);
            set => PlayerData.Write(OptionsKey.VsyncCount, value);
        }

        public override void PreInit()
        {
            nameText = Language.Read(OptionsKey.VsyncCount, "Vsync");
            ChangeVSyncCount();
        }

        public override void Prev() => SetVSyncCount(false);
        public override void Next() => SetVSyncCount(true);

        public void SetVSyncCount(bool mas)
        {
            if (mas) vsyncCount = (vsyncCount + 1) % 3;
            else if (vsyncCount != 0) vsyncCount = (vsyncCount - 1) % 3;
            else vsyncCount = 2;
            ChangeVSyncCount();
        }

        private void ChangeVSyncCount()
        {
            valueText = VSyncNames[vsyncCount].ToUpper();
            switch (vsyncCount)
            {
                case 0:
                    QualitySettings.vSyncCount = 0;
                    break;
                case 1:
                    QualitySettings.vSyncCount = 1;
                    break;
                case 2:
                    QualitySettings.vSyncCount = 2;
                    break;
            }
        }
        private string[] VSyncNames = new string[] { "Don't Sync", "Every V Blank", "Every Second V Blank" };
    }
}
