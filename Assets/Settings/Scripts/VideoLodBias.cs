using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoLodBias : MonoBehaviour
    {
        [Range(0.01f, 3)]
        public float defaultLoadBias = 1f;
        public float lodBias
        {
            get => PlayerData.Read(OptionsKey.LodBias, defaultLoadBias);
            set 
            {
                QualitySettings.lodBias = PlayerData.Write(OptionsKey.LodBias, value);
                valueText = $"{value.ToString("F2")}";
            }
        }

        private Text[] qualityText;
        private string nameText { get => qualityText[0].text; set => qualityText[0].text = value; }
        private string valueText { get => qualityText[1].text; set => qualityText[1].text = value; }
        private Slider distanceSlider;

        public void OnDisable() => QualitySettings.lodBias = lodBias;
        public void Awake()
        {
            qualityText = gameObject.GetComponentsInChildren<Text>();
            distanceSlider = gameObject.GetComponentInChildren<Slider>();
            distanceSlider.minValue = .01f;
            distanceSlider.maxValue = 3;
            nameText = Language.Read(OptionsKey.LodBias, "Distance Quality");
            distanceSlider.onValueChanged.AddListener((float value) => { lodBias = value; });
            distanceSlider.value = lodBias;
            valueText = $"{lodBias.ToString("F2")}";
        }
    }
}
