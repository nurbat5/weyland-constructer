using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoShadowEnable : BaseQualitySettings
    {
        public bool shadowEnable
        {
            get => PlayerData.Read(OptionsKey.ShadowEnable, false);
            set 
            {
                PlayerData.Write(OptionsKey.ShadowEnable, value);
                QualitySettings.shadowDistance = value ? PlayerData.Read(OptionsKey.ShadowDistance, 40f) : 0;
                valueText = value ? "ENABLE" : "DISABLE";

            }
        }

        public override void PreInit()
        {
            nameText = Language.Read(OptionsKey.ShadowEnable, "Shadow Enable");
            shadowEnable = shadowEnable;
        }

        public override void Prev() => shadowEnable = false;
        public override void Next() => shadowEnable = true;
    }
}
