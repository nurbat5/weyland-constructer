using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoResolution : BaseQualitySettings
    {
        [Range(0, 15)]
        public int defaultResolution = 7;
        public int resolution
        { 
            get => PlayerData.Read(OptionsKey.Resolution, defaultResolution); 
            set => PlayerData.Write(OptionsKey.Resolution, value); 
        }
        private bool currentFullScreen => PlayerData.Read(OptionsKey.ResolutionMode, false);//GameOptions.Instance.fullScreen.currentFullScreen;

        public override void Prev() => SetResolution(false);
        public override void Next () => SetResolution(true);


        public override void PreInit()
        {
            nameText = Language.Read(OptionsKey.Resolution, "Resolution");
            valueText = Screen.resolutions[resolution].width + " X " + Screen.resolutions[resolution].height;
            ApplyResolution();
        }

        public void SetResolution(bool mas)
        {
            if(mas) resolution = (resolution + 1) % Screen.resolutions.Length;
            else if(resolution != 0) resolution = (resolution - 1) % Screen.resolutions.Length;
            else resolution = (Screen.resolutions.Length - 1);
            valueText = Screen.resolutions[resolution].width + " X " + Screen.resolutions[resolution].height;
        }

        public void ApplyResolution()
        {
            Screen.SetResolution(Screen.resolutions[resolution].width, Screen.resolutions[resolution].height, currentFullScreen);
        }
    }
}
