﻿using System.Collections;
using System.Collections.Generic;
using Steamworks;
using UnityEngine;
using Weyland;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class SteamApplication : MonoBehaviour
{
    void Start()
    {
        Log.hasSave = true;
        try
        {
            SteamClient.Init(1060680);
        }
        catch (System.Exception e)
        {
            Application.Quit();
            Log.Write(e);
            // Couldn't init for some reason (steam is closed etc)
        }
    }


    public void Quit() => Application.Quit();
    void OnDestroy()
    {
        SteamClient.Shutdown();
    }
}
