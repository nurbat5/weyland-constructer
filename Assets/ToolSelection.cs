﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weyland.InventorySystem;

namespace Weyland
{
    public class ToolSelection : BackgroundEngine
    {
        public void Awake()
        {
            Inventory.elementOnChanged.AddListener(ToolsOnChanged);
        }

        private void Reset()
        {
            if (_currentTool == null) return;
            _currentTool.gameObject.SetActive(false);
            startupComponent.Clear();
            // startupComponent.Remove<ToolDelete>();
            _currentTool = null;
        }

        private Transform _currentTool;
        private void ToolsOnChanged(InventoryElementAsset asset)
        {
            Reset();
            if (asset != null && asset.categoryName == "Tools")
            {
                if ((_currentTool = transform.Find($"[{asset.name.ToUpper()}]")) != null)
                {
                    startupComponent.Add<ToolDelete>();
                    _currentTool.gameObject.SetActive(true);
                }
            }
        }
    }
}